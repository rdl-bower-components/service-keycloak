class KeycloakRBACService {
    constructor($injector, $state, $rootScope, $timeout, deniedState, resourcesAccess) {

        if (this.keycloakService === null) {//To avoid circular dependancies
            this.keycloakService = this.$injector.get('keycloakService');
        }
        this.keycloakService = null;
        this.resourcesAccess = resourcesAccess;
        this.$injector = $injector;
        this.$state = $state;
        this.$timeout = $timeout;
        this.$rootScope = $rootScope;
        this.deniedState = deniedState;
    }

    start() {
        this.$rootScope.$on('$stateChangeStart', (event, toState) => {
            try {
                this.checkAccess(toState.access);
            }
            catch (err) {
                console.log("Error during the role checking");
                this.$state.go(this.deniedState);
            }
        });

    }

    checkAccess (access) {
        if (this.keycloakService === null) {//To avoid circular dependancies
            this.keycloakService = this.$injector.get('keycloakService');
        }

        //Public access
        if (angular.isUndefined(access) || angular.isUndefined(access.requiredLogin) || !access.requiredLogin) {
            return;
        }
        //Authenticated but no required role
        if (this.keycloakService.isAuthenticated() && angular.isUndefined(access.requiredRole)) {
            return;
        }

        var roles = access.requiredRole.split('|');
        for (var i = 0 ; i < roles.length ; ++i) {
            var role = roles[i].replace(/{(.*)}/, (match, p1) => {
                return this.keycloakService.tokenParsed()[p1];
            });

            //Realm role access
            if (this.keycloakService.hasRealmRole(role)) {
                return;
            }

            //Client role access
            if (this.keycloakService.hasResourceRole(role)) {
                return;
            }
        };

        //If the user is authenticate, we go to the deniedState
        if (this.keycloakService.isAuthenticated()) {
            this.$timeout(() => {
                this.$state.go(this.deniedState);
            });
        }
        else {
            this.keycloakService.authenticate();
        }
    }

    hasAccessToResource (resource) {
        if (this.keycloakService === null) {//To avoid circular dependancies
            this.keycloakService = this.$injector.get('keycloakService');
        }

        var acceptedRoles = this.resourcesAccess[resource];
        if (angular.isUndefined (acceptedRoles)) {
            return false;
        }

        for (var i = 0 ; i < acceptedRoles.length ; ++i) {
            if (this.keycloakService.hasRealmRole(acceptedRoles[i])) {
                return true;
            }
        }
        return false;
    }
}

export class KeycloakRBACServiceProvider {
    constructor() {
        this.deniedState = '';
        this.resourcesAccess = {};
    }

    setDeniedState(deniedState) {
        this.deniedState = deniedState;
    }

    setResourcesAccess(resourcesAccess) {
        this.resourcesAccess = resourcesAccess;
    }

    $get ($injector, $state, $rootScope, $timeout) {
        'ngInject';
        return new KeycloakRBACService($injector, $state, $rootScope, $timeout, this.deniedState, this.resourcesAccess);
    }
}