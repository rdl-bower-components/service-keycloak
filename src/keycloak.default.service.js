export class KeycloakDefaultService {
    constructor($q, $rootScope, urls) {
        this.keycloak = window._keycloak;
        this.$q = $q;
        this.urls = urls;
        this.userInfo = {
            email: this.tokenParsed().email,
            firstName: this.tokenParsed().given_name,
            lastName: this.tokenParsed().family_name,
            pseudo: this.tokenParsed().preferred_username,
            id: this.keycloak.subject
        };

        this.$rootScope = $rootScope;
        this.keycloakUrl = this.keycloak.authServerUrl;
    }

    authenticate(option) {
        if (!this.keycloak.authenticated) {
            this.keycloak.login(option).success((response) => {
                this.onAuthSuccess();
            }).error((data, status) => {
                this.onAuthError();
            });
        }
        else {
            this.onAuthSuccess();
        }
    }

    onAuthSuccess () {
        this.userInfo.email = this.tokenParsed().email;
        this.userInfo.pseudo = this.tokenParsed().preferred_username;
        this.userInfo.firstName = this.tokenParsed().given_name;
        this.userInfo.lastName = this.tokenParsed().family_name;
        this.userInfo.id = this.keycloak.subject;

        this.$rootScope.$broadcast("keycloakAuthenticated");
        this.loadUserInfo();
    }

    onAuthError () {
        this.$rootScope.$broadcast("keycloakAuthenticationFailed");
    }

    getToken() {
        return this.keycloak.token;
    }

    getHeaders(addClientIdHeader = true) {
        var headers = {
            "Accept": "application/json",
            "Authorization": "bearer " + this.keycloak.token
        };
        if (addClientIdHeader) {
            headers['Client-Id'] = this.keycloak.clientId;
        }
        return headers;
    }

    isAuthenticated() {
        return this.keycloak.authenticated;
    }

    hasRealmRole(role) {
        return this.keycloak.hasRealmRole(role);
    }

    hasResourceRole(role, resource) {
        return this.keycloak.hasResourceRole(role, resource);
    }

    logout() {
        this.userInfo = {};
        this.keycloak.logout();
    }

    loadUserInfo() {
        this.keycloak.loadUserProfile().success((response) => {
            this.$rootScope.$apply(() => {
                this.userInfo.email = (response.email) ? response.email : "";
                this.userInfo.firstName = (response.firstName) ? response.firstName : "";
                this.userInfo.lastName = (response.lastName) ? response.lastName : "";
                this.userInfo.pseudo = (response.username) ? response.username : "";
                this.userInfo.id = (response.id) ? response.id : "";
                this.userInfo.attributes = (response.attributes) ? response.attributes : {};
            });
            this.$rootScope.$broadcast("keycloakInfoLoadedCallback", response);
        });
    }

    register() {
        this.keycloak.register().success((response) => {
            this.onAuthSuccess();
        }).error((data, status) => {
            this.onAuthError();
        });
    }

    tokenParsed() {
        if (angular.isDefined(this.keycloak.tokenParsed)) {
            return this.keycloak.tokenParsed;
        }
        return {};
    }

    updateToken(minValidity = 0) {
        var deferred = this.$q.defer();

        this.keycloak.updateToken(minValidity).success(() => {
            deferred.resolve();
        }).error(() => {
            this.$rootScope.$broadcast("keycloakTokenInvalid");
            deferred.reject();
        });

        return deferred.promise;
    }
}
