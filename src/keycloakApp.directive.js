export function keycloakApp(keycloakService, $compile) {
    return {
        terminal: true,
        compile: function(element, attrs) {
            var contents = element.contents();
            element.empty();
            return function postLink(scope, element) {
                keycloakService.startPromise.promise.then(() => {
                    element.append($compile(contents)(scope));
                });
            };
        }
    };
};