
export class KeycloakInterceptor {
    constructor($q, $injector, backendUrl) {
        this.$q = $q;
        this.backendUrl = backendUrl;
        this.$injector = $injector;
        this.keycloakService = null;

        return {
            request: this.request.bind(this)
        };
    }


    static create($q, $injector) {
        return new KeycloakInterceptor($q, $injector);
    }

    request (config) {
        if (this.keycloakService === null) {//To avoid circular dependancies
            this.keycloakService = this.$injector.get('keycloakService');
        }
        if (config.url.indexOf(this.backendUrl) > -1) {
            angular.extend(config.headers, this.keycloakService.getHeaders());
        }

        return config;
    }
}

export class KeycloakInterceptorProvider {
    constructor($httpProvider) {
        'ngInject';
        this.$httpProvider = $httpProvider;
        this.urls = [];
    }

    setBackendUrl(url) {
        this.backendUrl = url;
    }

    install() {
        this.$httpProvider.interceptors.push('keycloakInterceptor');
    }

    $get ($q, $injector) {
        'ngInject';
        return new KeycloakInterceptor($q, $injector, this.backendUrl);
    }
}
