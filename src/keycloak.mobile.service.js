export class KeycloakMobileService {
    constructor($http, $q, $rootScope, $state, $timeout, $ionicHistory, urls, clientId,
                clientSecret, afterLoginState, afterRegisterState, afterLogoutState, termsConditions, rememberMe, addEmailHint, socialNetworks) {

        this.urls = urls;
        this.clientId = clientId;
        this.rememberMe = rememberMe;
        this.addEmailHint = addEmailHint;
        this.termsConditions = termsConditions;
        this.clientSecret = clientSecret;
        this.afterLoginState = afterLoginState;
        this.afterRegisterState = afterRegisterState;
        this.afterLogoutState = afterLogoutState;
        this.$timeout = $timeout;
        this.$http = $http;
        this.$q = $q;
        this.$rootScope = $rootScope;
        this.$ionicHistory = $ionicHistory;
        this.$state = $state;
        this.socialNetworks = socialNetworks;
        this.startPromise = this.$q.defer();

        this.authenticated = false;

        this.kcToken = "";
        this.kcTokenParsed = {};
        this.userInfo = {};
        this.kcRefreshToken = "";
    }

    // ################## public ##################

    authenticate() {
        if (!this.authenticated) {
            this.loginMessage = undefined;
            this.$state.go("keycloakLogin")
        }
    }

    getHeaders(addClientIdHeader = true) {
        var headers = {
            "Accept": "application/json",
            "Authorization": "bearer " + this.kcToken
        };
        if (addClientIdHeader) {
            headers['Client-Id'] = this.clientId;
        }
        return headers;
    }

    getToken() {
        return this.kcToken;
    }

    hasRealmRole(role) {
        var access = this.kcTokenParsed.realm_access;
        return !!access && access.roles.indexOf(role) >= 0;
    }

    hasResourceRole(role, resource) {
        var access = this.kcTokenParsed.resource_access;
        if (!access) {
            return false;
        }
        access = kc.resourceAccess[resource || this.clientId];
        return !!access && access.roles.indexOf(role) >= 0;
    }

    isAuthenticated() {
        return this.authenticated;
    }

    logout() {
        var deferred = this.$q.defer();
        var param = "refresh_token=" + encodeURIComponent(this.kcRefreshToken);
        this.authenticated = false;
        this.userInfo = {};
        this.kcToken = "";
        this.kcTokenParsed = {};
        this.kcRefreshToken = "";
        this.timeSkew = 0;
        window.localStorage.removeItem('keycloakToken');
        window.localStorage.removeItem('keycloakRefreshToken');

        var req = new XMLHttpRequest();
        req.open('POST', this.urls.logout, true);
        req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        req.setRequestHeader('Authorization', 'Basic ' + btoa(this.clientId + ':' + this.clientSecret));
        req.withCredentials = true;

        req.onreadystatechange = () => {
            if (req.readyState == 4) {
                this.$rootScope.$broadcast("keycloakLogout");
                if (req.status == 200) {
                    deferred.resolve();
                } else {
                    deferred.reject();
                }
                var state = angular.isFunction(this.afterLogoutState) ? this.afterLogoutState() : this.afterLogoutState;
                this.go(state);
            }
        };
        req.send(param);
        return deferred.promise;
    }

    showLegalNotices() {
        this.$state.go("menu.keycloakTermsConditions");

    }

    register(user) {
        if (!this.authenticated) {
            this.loginMessage = undefined;
            this.$state.go("keycloakRegister");
        }
    }

    setAfterLoginState(afterLoginState) {
        this.afterLoginState = afterLoginState;
    }

    setAfterLogoutState(afterLogoutState) {
        this.afterLogoutState = afterLogoutState;
    }

    start() {
        var keycloakToken = window.localStorage.getItem("keycloakToken");
        var keycloakRefreshToken = window.localStorage.getItem("keycloakRefreshToken");

        if (keycloakRefreshToken === null || keycloakRefreshToken === 'undefined') {
            this.startPromise.resolve();
            return;
        }
        //If the access token is still valid, we use it
        var tokenParsed = this.decodeToken(keycloakRefreshToken);
        var timeSkew = Math.floor(new Date().getTime() / 1000) - tokenParsed.iat;
        if (!this.isTokenExpired(60, tokenParsed.exp, timeSkew)) {
            this.onAuthSuccess(keycloakToken, keycloakRefreshToken);
        }

        //If we know a refresh token, we try to use it to get a new access token
        var refreshTokenParsed = this.decodeToken(keycloakRefreshToken);
        timeSkew = Math.floor(new Date().getTime() / 1000) - refreshTokenParsed.iat;

        this.refreshToken(-1, refreshTokenParsed.exp, timeSkew, keycloakRefreshToken).finally(() => {
            this.startPromise.resolve();
        });
    }

    tokenParsed() {
        return this.kcTokenParsed;
    }

    updateToken(minValidity = -1) {
        return this.refreshToken(minValidity, this.kcTokenParsed.exp, this.timeSkew, this.kcRefreshToken);
    }

    // ################## private ##################

    createUser(user) {
        var deferred = this.$q.defer();
        if (user.pseudo === "" || user.pseudo === null) {
            user.pseudo = user.email;
        }
        var req = new XMLHttpRequest();
        req.open('POST', this.urls.register, true);
        req.setRequestHeader('Content-type', 'application/json');
        req.setRequestHeader('Client-Id', this.clientId);
        req.setRequestHeader('Authorization', 'Basic ' + btoa(this.clientId + ':' + this.clientSecret));
        req.withCredentials = true;

        req.onreadystatechange = () => {
            if (req.readyState == 4) {
                if (req.status == 200) {
                    deferred.resolve(JSON.parse(req.responseText));
                } else {
                    deferred.reject((req.responseText === '') ? '': JSON.parse(req.responseText));
                }
            }
        };
        req.send(JSON.stringify(user));

        return deferred.promise;
    }

    decodeToken(str) {
        str = str.split('.')[1];
        str = str.replace('/-/g', '+');
        str = str.replace('/_/g', '/');
        switch (str.length % 4)
        {
            case 0:
                break;
            case 2:
                str += '==';
                break;
            case 3:
                str += '=';
                break;
            default:
                throw 'Invalid token';
        }

        str = (str + '===').slice(0, str.length + (str.length % 4));
        str = str.replace(/-/g, '+').replace(/_/g, '/');

        str = decodeURIComponent(escape(atob(str)));

        str = JSON.parse(str);
        return str;
    }

    login(username, password, afterRegister) {
        var deferred = this.$q.defer();

        var req = new XMLHttpRequest();
        req.open('POST', this.urls.token, true);
        req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        req.setRequestHeader('Authorization', 'Basic ' + btoa(this.clientId + ':' + this.clientSecret));
        req.withCredentials = true;

        req.onreadystatechange = () => {
            if (req.readyState == 4) {
                if (req.status == 200) {
                    var data = JSON.parse(req.responseText);
                    this.onAuthSuccess(data.access_token, data.refresh_token, afterRegister);
                    deferred.resolve();
                } else {
                    deferred.reject((req.responseText === '') ? '': JSON.parse(req.responseText));
                }
            }
        };
        req.send('grant_type=password&username=' + encodeURIComponent(username) + '&password=' + encodeURIComponent(password));
        return deferred.promise;
    }


    forgotPassword(email) {
        var deferred = this.$q.defer();

        var req = new XMLHttpRequest();
        req.open('POST', this.urls.forgotPassword, true);
        req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        req.setRequestHeader('Authorization', 'bearer ' + this.token);
        req.withCredentials = true;

        req.onreadystatechange = () => {
            if (req.readyState == 4) {
                if (req.status == 200) {
                    var data = JSON.parse(req.responseText);
                    deferred.resolve();
                } else {
                    deferred.reject((req.responseText === '') ? '': JSON.parse(req.responseText));
                }
            }
        };
        req.send('email=' + encodeURIComponent(email));
        return deferred.promise;
    }

    isTokenExpired (minValidity, exp, timeSkew) {
        if (minValidity === -1) {
            return true;
        }
        var expiresIn = exp - Math.ceil(new Date().getTime() / 1000) + timeSkew;
        if (minValidity) {
            expiresIn -= minValidity;
        }
        return expiresIn < 0;
    }

    refreshToken(minValidity,  exp, timeSkew, refreshToken) {
        var deferred = this.$q.defer();

        //If the user is not authenticated
        if (refreshToken === '') {
            deferred.reject();
            return deferred.promise;
        }
        //If the access token is still valid
        if (!this.isTokenExpired(minValidity, exp, timeSkew)) {
            deferred.resolve();
            return deferred.promise;
        }

        var req = new XMLHttpRequest();
        req.open('POST', this.urls.token, true);
        req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        req.setRequestHeader('Authorization', 'Basic ' + btoa(this.clientId + ':' + this.clientSecret));
        req.withCredentials = true;
        req.timeout = 2000;

        req.onreadystatechange = () => {
            if (req.readyState == 4) {
                if (req.status == 200) {
                    var data = JSON.parse(req.responseText);
                    this.onAuthSuccess(data.access_token, data.refresh_token);
                    deferred.resolve();
                } else {
                    if (req.status >= 400 && req.status < 500) {
                        this.logout();
                    }
                    if (minValidity === -1 && req.status < 400) {
                        this.refreshToken(3600, exp, timeSkew, refreshToken).then(data => {
                            deferred.resolve();
                        }, () => {
                            deferred.reject();
                        })
                    }
                    else {
                        deferred.reject((req.responseText === '') ? '': JSON.parse(req.responseText));
                    }
                }
            }
        };
        req.send('grant_type=refresh_token&refresh_token=' + encodeURIComponent(refreshToken));
        return deferred.promise;
    }

    onAuthSuccess (token, refreshToken, afterRegister = false) {
        var broadcastSignal = (!this.authenticated) ? true : false;
        this.authenticated = true;
        this.kcToken = token;
        this.kcRefreshToken = refreshToken;
        this.kcTokenParsed = this.decodeToken(token);
        this.timeSkew = Math.floor(new Date().getTime() / 1000) - this.kcTokenParsed.iat;

        this.userInfo.email = this.kcTokenParsed.email;
        this.userInfo.pseudo = this.kcTokenParsed.preferred_username;
        this.userInfo.firstName = this.kcTokenParsed.given_name;
        this.userInfo.lastName = this.kcTokenParsed.family_name;
        this.userInfo.id = this.kcTokenParsed.sub;

        window.localStorage.setItem('keycloakToken', this.kcToken);
        window.localStorage.setItem('keycloakRefreshToken', this.kcRefreshToken);

        if (broadcastSignal) {
            if (this.termsConditions !== null) {
                this.checkTermsConditionsVersion(afterRegister);
            }
            else {
                this.showFirstState(afterRegister);
            }
        }
    }

    checkTermsConditionsVersion ( afterRegister = false) {
        var req = new XMLHttpRequest();
        req.open('GET', this.urls.termsConditionsVersion, true);

        req.onreadystatechange = () => {
            if (req.readyState == 4) {
                if (req.status == 200) {
                    var localVersion = window.localStorage.getItem("keycloakTermsConditionsValidatedVersion-" + this.userInfo.id);
                    var data = JSON.parse(req.responseText);
                    if (data[this.termsConditions.versionField] != localVersion) {
                        this.termsConditions.newVersion = data[this.termsConditions.versionField];
                        this.termsConditions.afterRegister = afterRegister;
                        this.go('keycloakTermsConditions');
                        return;
                    }
                }
                this.showFirstState(afterRegister);
            }
        };
        req.send();
    }

    showFirstState(afterRegister) {
        this.$rootScope.$broadcast("keycloakAuthenticated");
        var state = '';
        if (afterRegister) {
            state = angular.isFunction(this.afterRegisterState) ? this.afterRegisterState() : this.afterRegisterState;
        }
        else {
            state = angular.isFunction(this.afterLoginState) ? this.afterLoginState() : this.afterLoginState;
        }
        this.go(state);
    }

    go(state) {
        this.$timeout(() => {
            this.$ionicHistory.clearHistory();
            this.$ionicHistory.nextViewOptions({historyRoot: true, disableBack: true});
            this.$state.go(state);
        });
    }
}