export class ForgotPasswordController {
  constructor(keycloakService, $ionicHistory, $filter, $ionicLoading) {
    'ngInject'

    this.keycloakService = keycloakService;
    this.$ionicLoading = $ionicLoading;
    this.$filter = $filter;
    this.error = false;
    this.$ionicHistory = $ionicHistory;
  }

  forgotPassword() {
      var waiting = this.$filter('translate')('keycloak.waiting');
      this.$ionicLoading.show({template: waiting});
      this.keycloakService.forgotPassword(this.email).then(() => {
          this.$ionicLoading.hide();
          this.keycloakService.loginMessage = "emailSent";
          this.$ionicHistory.goBack();
      }, () => {
          this.$ionicLoading.hide();
          this.forgotForm.$setPristine();
          this.error = true;
      })
  }

}
