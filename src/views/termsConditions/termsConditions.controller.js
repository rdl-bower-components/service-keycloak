export class TermsConditionsController {
  constructor($http, keycloakService, $scope) {
    'ngInject'

      this.$http = $http;
      this.keycloakService = keycloakService;
      this.termsConditions = '';
      $scope.$on('$ionicView.enter', () => this.loadConditions());
  }

  loadConditions() {
      this.$http.get(this.keycloakService.urls.termsConditions).then( data => {
          this.termsConditions = data.data[this.keycloakService.termsConditions.dataField];
      });
  }

  accept() {
      window.localStorage.setItem('keycloakTermsConditionsValidatedVersion-' + this.keycloakService.userInfo.id, this.keycloakService.termsConditions.newVersion);
      this.keycloakService.showFirstState(this.keycloakService.termsConditions.afterRegister);
  }

  decline() {
      this.keycloakService.logout();
  }
}
