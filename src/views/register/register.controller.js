export class RegisterController {
  constructor(keycloakService, $ionicLoading, $filter) {
    'ngInject'

    this.$ionicLoading = $ionicLoading;
    this.$filter = $filter;
    this.keycloakService = keycloakService;
    this.confirmPassword = '';
    this.user = {
      firstName: "",
      lastName: "",
      email: "",
      pseudo: "",
      password: ""
    };
    this.error = false;
  }

  register() {
      if (this.user.password !== this.confirmPassword) {
        this.error = true;
        this.errorType = 'wrongConfPassword';
        return;
      }
      var waiting = this.$filter('translate')('keycloak.waiting');
      var user = angular.copy(this.user);
      this.$ionicLoading.show({template: waiting});
      this.keycloakService.createUser(user).then(() => {
        this.keycloakService.login(this.user.email, this.user.password, true).then(() => {
            this.$ionicLoading.hide();
        });
      }, error => {
          this.registerForm.$setPristine();
          this.$ionicLoading.hide();
          this.error = true;
          this.errorType = error.error;
      });
  }

}
