export class LoginController {
  constructor(keycloakService, $ionicLoading, $filter, $scope) {
    'ngInject'

    this.keycloakService = keycloakService;
    this.$ionicLoading = $ionicLoading;
    this.$filter = $filter;
    this.error = false;
    this.rememberMe = true;
    $scope.$on('$ionicView.enter', () => this.init());
  }

  init () {
      this.loginForm.$setPristine();
      if (this.keycloakService.rememberMe) {
          this.username = window.localStorage.getItem("keycloakUsername");
          this.password = window.localStorage.getItem("keycloakPassword");
      }
  }

  login(username, password) {
      var waiting = this.$filter('translate')('keycloak.waiting');
      this.$ionicLoading.show({template: waiting});
      this.keycloakService.login(username, password, false).then(() => {
          this.$ionicLoading.hide();
          if (this.rememberMe && this.keycloakService.rememberMe) {
              window.localStorage.setItem("keycloakUsername", username);
              window.localStorage.setItem("keycloakPassword", password);
          }
          else {
              window.localStorage.removeItem("keycloakUsername");
              window.localStorage.removeItem("keycloakPassword");
          }
      }, error => {
          this.loginForm.$setPristine();
          this.$ionicLoading.hide();
          this.error = true;
      });
  }

  loginWithFacebook() {
      facebookConnectPlugin.login(['public_profile', 'email'], res => {
          var waiting = this.$filter('translate')('keycloak.waiting');
          this.$ionicLoading.show({template: waiting});
          facebookConnectPlugin.api("/" + res.authResponse.userID + "?fields=id,email,currency,last_name,first_name,gender", ['public_profile', 'email'], info => {
              let loginInfo = {
                  token: res.authResponse.accessToken,
                  id: res.authResponse.userID,
                  email: info.email,
                  pseudo: info.email,
                  icon: "https://graph.facebook.com/" + res.authResponse.userID + "/picture?type=large",
                  currency: info.currency.user_currency,
                  lastName: info.last_name,
                  firstName: info.first_name,
                  gender: info.gender
              };
              this.checkSocialNetworkLogin(this.keycloakService.socialNetworks.facebook, loginInfo);
          });

      }, e => {
          console.log(e);
      });
  }

  loginWithLinkedin() {
      cordova.plugins.LinkedIn.login(['r_basicprofile', 'r_emailaddress'], true, () => {
          cordova.plugins.LinkedIn.getActiveSession(token => {
              cordova.plugins.LinkedIn.getRequest('people/~:(id,first-name,last-name,email-address,picture-urls::(original))', info => {
                  let loginInfo = {
                      token: token.accessToken,
                      id: info.id,
                      email: info.emailAddress,
                      pseudo: info.emailAddress,
                      icon: (info.pictureUrls.values.length > 0) ? info.pictureUrls.values[0]: '',
                      lastName: info.lastName,
                      firstName: info.firstName
                  };
                  this.checkSocialNetworkLogin(this.keycloakService.socialNetworks.linkedin, loginInfo);
              }, e => {
                  console.log(e);
              });
          }, e => {
              console.log(e);
          });
      }, e => {
          console.log(e);
      });
  }

  loginWithGoogle(socialNetwork) {
      window.plugins.googleplus.login({scopes: 'profile email'}, (res) => {
          if (angular.isUndefined(res.familyName)) {
              res.givenName = res.displayName.substr(0, res.displayName.indexOf(' '));
              res.familyName = res.displayName.substr(res.displayName.indexOf(' ') + 1);
          }
          let loginInfo = {
              token: res.accessToken,
              id: res.userId,
              email: res.email,
              pseudo: res.email,
              icon: res.imageUrl,
              lastName: res.familyName,
              firstName: res.givenName
          };
          this.checkSocialNetworkLogin(this.keycloakService.socialNetworks.google, loginInfo);
      },  e => {
          console.log(e);
      });
  }

  loginWithTwitter(socialNetwork) {
      TwitterConnect.login(res => {
          console.log(res);
          let req = new XMLHttpRequest();
          req.open('POST', this.keycloakService.urls.twitterInfo, true);
          req.setRequestHeader('Client-Id', this.keycloakService.clientId);
          req.setRequestHeader('Content-type', 'application/json');
          req.withCredentials = true;

          req.onreadystatechange = () => {
              if (req.readyState == 4) {
                  if (req.status == 200) {
                      let twitterInfo = JSON.parse(req.responseText);
                      let loginInfo = {
                          token: res.token,
                          id: res.userId,
                          email: twitterInfo.email,
                          pseudo: twitterInfo.email,
                          icon: twitterInfo.icon,
                          lastName: twitterInfo.name.substr(twitterInfo.name.indexOf(' ') + 1),
                          firstName: twitterInfo.name.substr(0, twitterInfo.name.indexOf(' '))
                      };
                      this.checkSocialNetworkLogin(this.keycloakService.socialNetworks.twitter, loginInfo);
                  } else {
                      console.log('error :' + (req.responseText === '') ? '': JSON.parse(req.responseText));
                  }
              }
          };
          req.send(JSON.stringify({
              token: res.token,
              secret: res.secret
          }));
      }, e => {
          console.log(e);
      });
  }

  checkSocialNetworkLogin(socialNetwork, loginInfo) {
      let req = new XMLHttpRequest();
      req.open('POST', this.keycloakService.urls.socialLogin + "/" + socialNetwork.key, true);
      req.setRequestHeader('Content-type', 'application/json');
      req.setRequestHeader('Client-Id', this.keycloakService.clientId);
      req.withCredentials = true;

      req.onreadystatechange = () => {
          if (req.readyState == 4) {
              if (req.status == 200) {
                  this.login(loginInfo.email, loginInfo.token);
              } else {
                  console.log('error :' + (req.responseText === '') ? '': JSON.parse(req.responseText));
              }
          }
      };
      req.send(JSON.stringify(loginInfo));
  }

}
