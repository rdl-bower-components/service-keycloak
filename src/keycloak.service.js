
import {KeycloakInterceptorProvider} from './keycloakInterceptor.factory';
import {KeycloakRBACServiceProvider} from './keycloakRBAC.service';
import {keycloakHasAccess} from './keycloakHasAccess.directive';
import {keycloakApp} from './keycloakApp.directive';
import {KeycloakMobileService} from './keycloak.mobile.service';
import {KeycloakDefaultService} from './keycloak.default.service';
import {LoginController} from './views/login/login.controller';
import {RegisterController} from './views/register/register.controller';
import {ForgotPasswordController} from './views/forgotPassword/forgotPassword.controller';
import {TermsConditionsController} from './views/termsConditions/termsConditions.controller';


export class KeycloakServiceProvider {
    constructor($stateProvider) {
        'ngInject';
        this.socialNetworks = null;
        this.mobileMode = false;
        this.rememberMe = false;
        this.termsConditions = null;
        this.$stateProvider = $stateProvider;
    }

    activeRememberMe() {
        this.rememberMe = true;
    }


    activeEmailHint() {
        this.addEmailHint = true;
    }

    activeTermsConditionsView(dataField, versionField) {
        this.termsConditions = {
            dataField: dataField,
            versionField: versionField
        };
    }

    activeFacebook() {
        if (this.socialNetworks === null) {
            this.socialNetworks = {};
        }
        this.socialNetworks.facebook = {
            key: 'facebook',
            image: 'assets/images/keycloak/facebook.png'
        };
    }

    activeGoogle() {
        if (this.socialNetworks === null) {
            this.socialNetworks = {};
        }
        this.socialNetworks.google = {
            key: 'google',
            image: 'assets/images/keycloak/google.png'
        };
    }

    activeTwitter() {
        if (this.socialNetworks === null) {
            this.socialNetworks = {};
        }
        this.socialNetworks.twitter = {
            key: 'twitter',
            image: 'assets/images/keycloak/twitter.png'
        };
    }

    activeLinkedin() {
        if (this.socialNetworks === null) {
            this.socialNetworks = {};
        }
        this.socialNetworks.linkedin = {
            key: 'linkedin',
            image: 'assets/images/keycloak/linkedin.png'
        };
    }

    setUrls(urls) {
        this.urls = urls;
    }

    setKeycloakUrl(url) {
        this.keycloakUrl = url.replace(/\/$/, '');
    }

    setClientId(clientId) {
        this.clientId = clientId;
    }

    setClientSecret(clientSecret) {
        this.clientSecret = clientSecret;
    }

    setAfterLoginState(afterLoginState) {
        this.afterLoginState = afterLoginState;
    }

    setAfterRegisterState(afterRegisterState) {
        this.afterRegisterState = afterRegisterState;
    }

    setAfterLogoutState(afterLogoutState) {
        this.afterLogoutState = afterLogoutState;
    }

    setMobileMode(mobileMode) {
        this.mobileMode = mobileMode;
        if (mobileMode) {
            this.$stateProvider
                .state('keycloakLogin', {
                    url: '/keycloak/login',
                    templateUrl: 'views/login/login.html',
                    controller: 'LoginController',
                    controllerAs: 'ctrl'
                })
                .state('keycloakRegister', {
                    url: '/keycloak/register',
                    templateUrl: 'views/register/register.html',
                    controller: 'RegisterController',
                    controllerAs: 'ctrl'
                })
                .state('keycloakForgotPassword', {
                    url: '/keycloak/forgotPassword',
                    templateUrl: 'views/forgotPassword/forgotPassword.html',
                    controller: 'ForgotPasswordController',
                    controllerAs: 'ctrl'
                })
                .state('keycloakTermsConditions', {
                    url: '/keycloak/termsConditions',
                    templateUrl: 'views/termsConditions/termsConditions.html',
                    controller: 'TermsConditionsController',
                    controllerAs: 'ctrl'
                })

                .state('menu.keycloakTermsConditions', {
                    url: '/keycloak/termsConditionsWithMenu',
                    views: {
                        'menuContent': {
                            templateUrl: 'views/termsConditions/termsConditionsWithMenu.html',
                            controller: 'TermsConditionsController',
                            controllerAs: 'ctrl'
                        }
                    }
                })
            ;
        }
    }

    $get ($http, $q, $rootScope, $state, $timeout, $injector) {
        'ngInject';
        if (this.mobileMode) {
            return new KeycloakMobileService($http, $q, $rootScope, $state, $timeout, $injector.get('$ionicHistory'), this.urls,
                                             this.clientId, this.clientSecret, this.afterLoginState, this.afterRegisterState,
                                             this.afterLogoutState, this.termsConditions, this.rememberMe, this.addEmailHint, this.socialNetworks);
        }
        return new KeycloakDefaultService($q, $rootScope, this.urls);
    }
}


angular.module('rdl.keycloak', [])
    .directive('rdlHasAccess', keycloakHasAccess)
    .directive('rdlKeycloakApp', keycloakApp)
    .provider('keycloakService', KeycloakServiceProvider)
    .provider('keycloakRBACService', KeycloakRBACServiceProvider)
    .provider('keycloakInterceptor', KeycloakInterceptorProvider)
    .controller('LoginController', LoginController)
    .controller('RegisterController', RegisterController)
    .controller('ForgotPasswordController', ForgotPasswordController)
    .controller('TermsConditionsController', TermsConditionsController)
;
