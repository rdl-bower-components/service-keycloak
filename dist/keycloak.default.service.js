/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(2);


/***/ },
/* 1 */,
/* 2 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var KeycloakDefaultService = exports.KeycloakDefaultService = function () {
	    function KeycloakDefaultService($q, $rootScope, urls) {
	        _classCallCheck(this, KeycloakDefaultService);

	        this.keycloak = window._keycloak;
	        this.$q = $q;
	        this.urls = urls;
	        this.userInfo = {
	            email: this.tokenParsed().email,
	            firstName: this.tokenParsed().given_name,
	            lastName: this.tokenParsed().family_name,
	            pseudo: this.tokenParsed().preferred_username,
	            id: this.keycloak.subject
	        };

	        this.$rootScope = $rootScope;
	        this.keycloakUrl = this.keycloak.authServerUrl;
	    }

	    _createClass(KeycloakDefaultService, [{
	        key: "authenticate",
	        value: function authenticate(option) {
	            var _this = this;

	            if (!this.keycloak.authenticated) {
	                this.keycloak.login(option).success(function (response) {
	                    _this.onAuthSuccess();
	                }).error(function (data, status) {
	                    _this.onAuthError();
	                });
	            } else {
	                this.onAuthSuccess();
	            }
	        }
	    }, {
	        key: "onAuthSuccess",
	        value: function onAuthSuccess() {
	            this.userInfo.email = this.tokenParsed().email;
	            this.userInfo.pseudo = this.tokenParsed().preferred_username;
	            this.userInfo.firstName = this.tokenParsed().given_name;
	            this.userInfo.lastName = this.tokenParsed().family_name;
	            this.userInfo.id = this.keycloak.subject;

	            this.$rootScope.$broadcast("keycloakAuthenticated");
	            this.loadUserInfo();
	        }
	    }, {
	        key: "onAuthError",
	        value: function onAuthError() {
	            this.$rootScope.$broadcast("keycloakAuthenticationFailed");
	        }
	    }, {
	        key: "getToken",
	        value: function getToken() {
	            return this.keycloak.token;
	        }
	    }, {
	        key: "getHeaders",
	        value: function getHeaders() {
	            var addClientIdHeader = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	            var headers = {
	                "Accept": "application/json",
	                "Authorization": "bearer " + this.keycloak.token
	            };
	            if (addClientIdHeader) {
	                headers['Client-Id'] = this.keycloak.clientId;
	            }
	            return headers;
	        }
	    }, {
	        key: "isAuthenticated",
	        value: function isAuthenticated() {
	            return this.keycloak.authenticated;
	        }
	    }, {
	        key: "hasRealmRole",
	        value: function hasRealmRole(role) {
	            return this.keycloak.hasRealmRole(role);
	        }
	    }, {
	        key: "hasResourceRole",
	        value: function hasResourceRole(role, resource) {
	            return this.keycloak.hasResourceRole(role, resource);
	        }
	    }, {
	        key: "logout",
	        value: function logout() {
	            this.userInfo = {};
	            this.keycloak.logout();
	        }
	    }, {
	        key: "loadUserInfo",
	        value: function loadUserInfo() {
	            var _this2 = this;

	            this.keycloak.loadUserProfile().success(function (response) {
	                _this2.$rootScope.$apply(function () {
	                    _this2.userInfo.email = response.email ? response.email : "";
	                    _this2.userInfo.firstName = response.firstName ? response.firstName : "";
	                    _this2.userInfo.lastName = response.lastName ? response.lastName : "";
	                    _this2.userInfo.pseudo = response.username ? response.username : "";
	                    _this2.userInfo.id = response.id ? response.id : "";
	                    _this2.userInfo.attributes = response.attributes ? response.attributes : {};
	                });
	                _this2.$rootScope.$broadcast("keycloakInfoLoadedCallback", response);
	            });
	        }
	    }, {
	        key: "register",
	        value: function register() {
	            var _this3 = this;

	            this.keycloak.register().success(function (response) {
	                _this3.onAuthSuccess();
	            }).error(function (data, status) {
	                _this3.onAuthError();
	            });
	        }
	    }, {
	        key: "tokenParsed",
	        value: function tokenParsed() {
	            if (angular.isDefined(this.keycloak.tokenParsed)) {
	                return this.keycloak.tokenParsed;
	            }
	            return {};
	        }
	    }, {
	        key: "updateToken",
	        value: function updateToken() {
	            var _this4 = this;

	            var minValidity = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;

	            var deferred = this.$q.defer();

	            this.keycloak.updateToken(minValidity).success(function () {
	                deferred.resolve();
	            }).error(function () {
	                _this4.$rootScope.$broadcast("keycloakTokenInvalid");
	                deferred.reject();
	            });

	            return deferred.promise;
	        }
	    }]);

	    return KeycloakDefaultService;
	}();

/***/ }
/******/ ]);