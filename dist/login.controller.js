/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(9);


/***/ },

/***/ 9:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var LoginController = exports.LoginController = function () {
	    LoginController.$inject = ["keycloakService", "$ionicLoading", "$filter", "$scope"];
	    function LoginController(keycloakService, $ionicLoading, $filter, $scope) {
	        'ngInject';

	        var _this = this;

	        _classCallCheck(this, LoginController);

	        this.keycloakService = keycloakService;
	        this.$ionicLoading = $ionicLoading;
	        this.$filter = $filter;
	        this.error = false;
	        this.rememberMe = true;
	        $scope.$on('$ionicView.enter', function () {
	            return _this.init();
	        });
	    }

	    _createClass(LoginController, [{
	        key: 'init',
	        value: function init() {
	            this.loginForm.$setPristine();
	            if (this.keycloakService.rememberMe) {
	                this.username = window.localStorage.getItem("keycloakUsername");
	                this.password = window.localStorage.getItem("keycloakPassword");
	            }
	        }
	    }, {
	        key: 'login',
	        value: function login(username, password) {
	            var _this2 = this;

	            var waiting = this.$filter('translate')('keycloak.waiting');
	            this.$ionicLoading.show({ template: waiting });
	            this.keycloakService.login(username, password, false).then(function () {
	                _this2.$ionicLoading.hide();
	                if (_this2.rememberMe && _this2.keycloakService.rememberMe) {
	                    window.localStorage.setItem("keycloakUsername", username);
	                    window.localStorage.setItem("keycloakPassword", password);
	                } else {
	                    window.localStorage.removeItem("keycloakUsername");
	                    window.localStorage.removeItem("keycloakPassword");
	                }
	            }, function (error) {
	                _this2.loginForm.$setPristine();
	                _this2.$ionicLoading.hide();
	                _this2.error = true;
	            });
	        }
	    }, {
	        key: 'loginWithFacebook',
	        value: function loginWithFacebook() {
	            var _this3 = this;

	            facebookConnectPlugin.login(['public_profile', 'email'], function (res) {
	                var waiting = _this3.$filter('translate')('keycloak.waiting');
	                _this3.$ionicLoading.show({ template: waiting });
	                facebookConnectPlugin.api("/" + res.authResponse.userID + "?fields=id,email,currency,last_name,first_name,gender", ['public_profile', 'email'], function (info) {
	                    var loginInfo = {
	                        token: res.authResponse.accessToken,
	                        id: res.authResponse.userID,
	                        email: info.email,
	                        pseudo: info.email,
	                        icon: "https://graph.facebook.com/" + res.authResponse.userID + "/picture?type=large",
	                        currency: info.currency.user_currency,
	                        lastName: info.last_name,
	                        firstName: info.first_name,
	                        gender: info.gender
	                    };
	                    _this3.checkSocialNetworkLogin(_this3.keycloakService.socialNetworks.facebook, loginInfo);
	                });
	            }, function (e) {
	                console.log(e);
	            });
	        }
	    }, {
	        key: 'loginWithLinkedin',
	        value: function loginWithLinkedin() {
	            var _this4 = this;

	            cordova.plugins.LinkedIn.login(['r_basicprofile', 'r_emailaddress'], true, function () {
	                cordova.plugins.LinkedIn.getActiveSession(function (token) {
	                    cordova.plugins.LinkedIn.getRequest('people/~:(id,first-name,last-name,email-address,picture-urls::(original))', function (info) {
	                        var loginInfo = {
	                            token: token.accessToken,
	                            id: info.id,
	                            email: info.emailAddress,
	                            pseudo: info.emailAddress,
	                            icon: info.pictureUrls.values.length > 0 ? info.pictureUrls.values[0] : '',
	                            lastName: info.lastName,
	                            firstName: info.firstName
	                        };
	                        _this4.checkSocialNetworkLogin(_this4.keycloakService.socialNetworks.linkedin, loginInfo);
	                    }, function (e) {
	                        console.log(e);
	                    });
	                }, function (e) {
	                    console.log(e);
	                });
	            }, function (e) {
	                console.log(e);
	            });
	        }
	    }, {
	        key: 'loginWithGoogle',
	        value: function loginWithGoogle(socialNetwork) {
	            var _this5 = this;

	            window.plugins.googleplus.login({ scopes: 'profile email' }, function (res) {
	                if (angular.isUndefined(res.familyName)) {
	                    res.givenName = res.displayName.substr(0, res.displayName.indexOf(' '));
	                    res.familyName = res.displayName.substr(res.displayName.indexOf(' ') + 1);
	                }
	                var loginInfo = {
	                    token: res.accessToken,
	                    id: res.userId,
	                    email: res.email,
	                    pseudo: res.email,
	                    icon: res.imageUrl,
	                    lastName: res.familyName,
	                    firstName: res.givenName
	                };
	                _this5.checkSocialNetworkLogin(_this5.keycloakService.socialNetworks.google, loginInfo);
	            }, function (e) {
	                console.log(e);
	            });
	        }
	    }, {
	        key: 'loginWithTwitter',
	        value: function loginWithTwitter(socialNetwork) {
	            var _this6 = this;

	            TwitterConnect.login(function (res) {
	                console.log(res);
	                var req = new XMLHttpRequest();
	                req.open('POST', _this6.keycloakService.urls.twitterInfo, true);
	                req.setRequestHeader('Client-Id', _this6.keycloakService.clientId);
	                req.setRequestHeader('Content-type', 'application/json');
	                req.withCredentials = true;

	                req.onreadystatechange = function () {
	                    if (req.readyState == 4) {
	                        if (req.status == 200) {
	                            var twitterInfo = JSON.parse(req.responseText);
	                            var loginInfo = {
	                                token: res.token,
	                                id: res.userId,
	                                email: twitterInfo.email,
	                                pseudo: twitterInfo.email,
	                                icon: twitterInfo.icon,
	                                lastName: twitterInfo.name.substr(twitterInfo.name.indexOf(' ') + 1),
	                                firstName: twitterInfo.name.substr(0, twitterInfo.name.indexOf(' '))
	                            };
	                            _this6.checkSocialNetworkLogin(_this6.keycloakService.socialNetworks.twitter, loginInfo);
	                        } else {
	                            console.log( true ? '' : JSON.parse(req.responseText));
	                        }
	                    }
	                };
	                req.send(JSON.stringify({
	                    token: res.token,
	                    secret: res.secret
	                }));
	            }, function (e) {
	                console.log(e);
	            });
	        }
	    }, {
	        key: 'checkSocialNetworkLogin',
	        value: function checkSocialNetworkLogin(socialNetwork, loginInfo) {
	            var _this7 = this;

	            var req = new XMLHttpRequest();
	            req.open('POST', this.keycloakService.urls.socialLogin + "/" + socialNetwork.key, true);
	            req.setRequestHeader('Content-type', 'application/json');
	            req.setRequestHeader('Client-Id', this.keycloakService.clientId);
	            req.withCredentials = true;

	            req.onreadystatechange = function () {
	                if (req.readyState == 4) {
	                    if (req.status == 200) {
	                        _this7.login(loginInfo.email, loginInfo.token);
	                    } else {
	                        console.log( true ? '' : JSON.parse(req.responseText));
	                    }
	                }
	            };
	            req.send(JSON.stringify(loginInfo));
	        }
	    }]);

	    return LoginController;
	}();

/***/ }

/******/ });