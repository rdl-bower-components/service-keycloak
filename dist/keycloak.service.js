/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(4);


/***/ },
/* 1 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var ForgotPasswordController = exports.ForgotPasswordController = function () {
	    ForgotPasswordController.$inject = ["keycloakService", "$ionicHistory", "$filter", "$ionicLoading"];
	    function ForgotPasswordController(keycloakService, $ionicHistory, $filter, $ionicLoading) {
	        'ngInject';

	        _classCallCheck(this, ForgotPasswordController);

	        this.keycloakService = keycloakService;
	        this.$ionicLoading = $ionicLoading;
	        this.$filter = $filter;
	        this.error = false;
	        this.$ionicHistory = $ionicHistory;
	    }

	    _createClass(ForgotPasswordController, [{
	        key: 'forgotPassword',
	        value: function forgotPassword() {
	            var _this = this;

	            var waiting = this.$filter('translate')('keycloak.waiting');
	            this.$ionicLoading.show({ template: waiting });
	            this.keycloakService.forgotPassword(this.email).then(function () {
	                _this.$ionicLoading.hide();
	                _this.keycloakService.loginMessage = "emailSent";
	                _this.$ionicHistory.goBack();
	            }, function () {
	                _this.$ionicLoading.hide();
	                _this.forgotForm.$setPristine();
	                _this.error = true;
	            });
	        }
	    }]);

	    return ForgotPasswordController;
	}();

/***/ },
/* 2 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var KeycloakDefaultService = exports.KeycloakDefaultService = function () {
	    function KeycloakDefaultService($q, $rootScope, urls) {
	        _classCallCheck(this, KeycloakDefaultService);

	        this.keycloak = window._keycloak;
	        this.$q = $q;
	        this.urls = urls;
	        this.userInfo = {
	            email: this.tokenParsed().email,
	            firstName: this.tokenParsed().given_name,
	            lastName: this.tokenParsed().family_name,
	            pseudo: this.tokenParsed().preferred_username,
	            id: this.keycloak.subject
	        };

	        this.$rootScope = $rootScope;
	        this.keycloakUrl = this.keycloak.authServerUrl;
	    }

	    _createClass(KeycloakDefaultService, [{
	        key: "authenticate",
	        value: function authenticate(option) {
	            var _this = this;

	            if (!this.keycloak.authenticated) {
	                this.keycloak.login(option).success(function (response) {
	                    _this.onAuthSuccess();
	                }).error(function (data, status) {
	                    _this.onAuthError();
	                });
	            } else {
	                this.onAuthSuccess();
	            }
	        }
	    }, {
	        key: "onAuthSuccess",
	        value: function onAuthSuccess() {
	            this.userInfo.email = this.tokenParsed().email;
	            this.userInfo.pseudo = this.tokenParsed().preferred_username;
	            this.userInfo.firstName = this.tokenParsed().given_name;
	            this.userInfo.lastName = this.tokenParsed().family_name;
	            this.userInfo.id = this.keycloak.subject;

	            this.$rootScope.$broadcast("keycloakAuthenticated");
	            this.loadUserInfo();
	        }
	    }, {
	        key: "onAuthError",
	        value: function onAuthError() {
	            this.$rootScope.$broadcast("keycloakAuthenticationFailed");
	        }
	    }, {
	        key: "getToken",
	        value: function getToken() {
	            return this.keycloak.token;
	        }
	    }, {
	        key: "getHeaders",
	        value: function getHeaders() {
	            var addClientIdHeader = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	            var headers = {
	                "Accept": "application/json",
	                "Authorization": "bearer " + this.keycloak.token
	            };
	            if (addClientIdHeader) {
	                headers['Client-Id'] = this.keycloak.clientId;
	            }
	            return headers;
	        }
	    }, {
	        key: "isAuthenticated",
	        value: function isAuthenticated() {
	            return this.keycloak.authenticated;
	        }
	    }, {
	        key: "hasRealmRole",
	        value: function hasRealmRole(role) {
	            return this.keycloak.hasRealmRole(role);
	        }
	    }, {
	        key: "hasResourceRole",
	        value: function hasResourceRole(role, resource) {
	            return this.keycloak.hasResourceRole(role, resource);
	        }
	    }, {
	        key: "logout",
	        value: function logout() {
	            this.userInfo = {};
	            this.keycloak.logout();
	        }
	    }, {
	        key: "loadUserInfo",
	        value: function loadUserInfo() {
	            var _this2 = this;

	            this.keycloak.loadUserProfile().success(function (response) {
	                _this2.$rootScope.$apply(function () {
	                    _this2.userInfo.email = response.email ? response.email : "";
	                    _this2.userInfo.firstName = response.firstName ? response.firstName : "";
	                    _this2.userInfo.lastName = response.lastName ? response.lastName : "";
	                    _this2.userInfo.pseudo = response.username ? response.username : "";
	                    _this2.userInfo.id = response.id ? response.id : "";
	                    _this2.userInfo.attributes = response.attributes ? response.attributes : {};
	                });
	                _this2.$rootScope.$broadcast("keycloakInfoLoadedCallback", response);
	            });
	        }
	    }, {
	        key: "register",
	        value: function register() {
	            var _this3 = this;

	            this.keycloak.register().success(function (response) {
	                _this3.onAuthSuccess();
	            }).error(function (data, status) {
	                _this3.onAuthError();
	            });
	        }
	    }, {
	        key: "tokenParsed",
	        value: function tokenParsed() {
	            if (angular.isDefined(this.keycloak.tokenParsed)) {
	                return this.keycloak.tokenParsed;
	            }
	            return {};
	        }
	    }, {
	        key: "updateToken",
	        value: function updateToken() {
	            var _this4 = this;

	            var minValidity = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;

	            var deferred = this.$q.defer();

	            this.keycloak.updateToken(minValidity).success(function () {
	                deferred.resolve();
	            }).error(function () {
	                _this4.$rootScope.$broadcast("keycloakTokenInvalid");
	                deferred.reject();
	            });

	            return deferred.promise;
	        }
	    }]);

	    return KeycloakDefaultService;
	}();

/***/ },
/* 3 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var KeycloakMobileService = function () {
	    function KeycloakMobileService($http, $q, $rootScope, $state, $timeout, $ionicHistory, urls, clientId, clientSecret, afterLoginState, afterRegisterState, afterLogoutState, termsConditions, rememberMe, addEmailHint, socialNetworks) {
	        _classCallCheck(this, KeycloakMobileService);

	        this.urls = urls;
	        this.clientId = clientId;
	        this.rememberMe = rememberMe;
	        this.addEmailHint = addEmailHint;
	        this.termsConditions = termsConditions;
	        this.clientSecret = clientSecret;
	        this.afterLoginState = afterLoginState;
	        this.afterRegisterState = afterRegisterState;
	        this.afterLogoutState = afterLogoutState;
	        this.$timeout = $timeout;
	        this.$http = $http;
	        this.$q = $q;
	        this.$rootScope = $rootScope;
	        this.$ionicHistory = $ionicHistory;
	        this.$state = $state;
	        this.socialNetworks = socialNetworks;
	        this.startPromise = this.$q.defer();

	        this.authenticated = false;

	        this.kcToken = "";
	        this.kcTokenParsed = {};
	        this.userInfo = {};
	        this.kcRefreshToken = "";
	    }

	    // ################## public ##################

	    _createClass(KeycloakMobileService, [{
	        key: "authenticate",
	        value: function authenticate() {
	            if (!this.authenticated) {
	                this.loginMessage = undefined;
	                this.$state.go("keycloakLogin");
	            }
	        }
	    }, {
	        key: "getHeaders",
	        value: function getHeaders() {
	            var addClientIdHeader = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	            var headers = {
	                "Accept": "application/json",
	                "Authorization": "bearer " + this.kcToken
	            };
	            if (addClientIdHeader) {
	                headers['Client-Id'] = this.clientId;
	            }
	            return headers;
	        }
	    }, {
	        key: "getToken",
	        value: function getToken() {
	            return this.kcToken;
	        }
	    }, {
	        key: "hasRealmRole",
	        value: function hasRealmRole(role) {
	            var access = this.kcTokenParsed.realm_access;
	            return !!access && access.roles.indexOf(role) >= 0;
	        }
	    }, {
	        key: "hasResourceRole",
	        value: function hasResourceRole(role, resource) {
	            var access = this.kcTokenParsed.resource_access;
	            if (!access) {
	                return false;
	            }
	            access = kc.resourceAccess[resource || this.clientId];
	            return !!access && access.roles.indexOf(role) >= 0;
	        }
	    }, {
	        key: "isAuthenticated",
	        value: function isAuthenticated() {
	            return this.authenticated;
	        }
	    }, {
	        key: "logout",
	        value: function logout() {
	            var _this = this;

	            var deferred = this.$q.defer();
	            var param = "refresh_token=" + encodeURIComponent(this.kcRefreshToken);
	            this.authenticated = false;
	            this.userInfo = {};
	            this.kcToken = "";
	            this.kcTokenParsed = {};
	            this.kcRefreshToken = "";
	            this.timeSkew = 0;
	            window.localStorage.removeItem('keycloakToken');
	            window.localStorage.removeItem('keycloakRefreshToken');

	            var req = new XMLHttpRequest();
	            req.open('POST', this.urls.logout, true);
	            req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	            req.setRequestHeader('Authorization', 'Basic ' + btoa(this.clientId + ':' + this.clientSecret));
	            req.withCredentials = true;

	            req.onreadystatechange = function () {
	                if (req.readyState == 4) {
	                    _this.$rootScope.$broadcast("keycloakLogout");
	                    if (req.status == 200) {
	                        deferred.resolve();
	                    } else {
	                        deferred.reject();
	                    }
	                    var state = angular.isFunction(_this.afterLogoutState) ? _this.afterLogoutState() : _this.afterLogoutState;
	                    _this.go(state);
	                }
	            };
	            req.send(param);
	            return deferred.promise;
	        }
	    }, {
	        key: "showLegalNotices",
	        value: function showLegalNotices() {
	            this.$state.go("menu.keycloakTermsConditions");
	        }
	    }, {
	        key: "register",
	        value: function register(user) {
	            if (!this.authenticated) {
	                this.loginMessage = undefined;
	                this.$state.go("keycloakRegister");
	            }
	        }
	    }, {
	        key: "setAfterLoginState",
	        value: function setAfterLoginState(afterLoginState) {
	            this.afterLoginState = afterLoginState;
	        }
	    }, {
	        key: "setAfterLogoutState",
	        value: function setAfterLogoutState(afterLogoutState) {
	            this.afterLogoutState = afterLogoutState;
	        }
	    }, {
	        key: "start",
	        value: function start() {
	            var _this2 = this;

	            var keycloakToken = window.localStorage.getItem("keycloakToken");
	            var keycloakRefreshToken = window.localStorage.getItem("keycloakRefreshToken");

	            if (keycloakRefreshToken === null || keycloakRefreshToken === 'undefined') {
	                this.startPromise.resolve();
	                return;
	            }
	            //If the access token is still valid, we use it
	            var tokenParsed = this.decodeToken(keycloakRefreshToken);
	            var timeSkew = Math.floor(new Date().getTime() / 1000) - tokenParsed.iat;
	            if (!this.isTokenExpired(60, tokenParsed.exp, timeSkew)) {
	                this.onAuthSuccess(keycloakToken, keycloakRefreshToken);
	            }

	            //If we know a refresh token, we try to use it to get a new access token
	            var refreshTokenParsed = this.decodeToken(keycloakRefreshToken);
	            timeSkew = Math.floor(new Date().getTime() / 1000) - refreshTokenParsed.iat;

	            this.refreshToken(-1, refreshTokenParsed.exp, timeSkew, keycloakRefreshToken).finally(function () {
	                _this2.startPromise.resolve();
	            });
	        }
	    }, {
	        key: "tokenParsed",
	        value: function tokenParsed() {
	            return this.kcTokenParsed;
	        }
	    }, {
	        key: "updateToken",
	        value: function updateToken() {
	            var minValidity = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : -1;

	            return this.refreshToken(minValidity, this.kcTokenParsed.exp, this.timeSkew, this.kcRefreshToken);
	        }

	        // ################## private ##################

	    }, {
	        key: "createUser",
	        value: function createUser(user) {
	            var deferred = this.$q.defer();
	            if (user.pseudo === "" || user.pseudo === null) {
	                user.pseudo = user.email;
	            }
	            var req = new XMLHttpRequest();
	            req.open('POST', this.urls.register, true);
	            req.setRequestHeader('Content-type', 'application/json');
	            req.setRequestHeader('Client-Id', this.clientId);
	            req.setRequestHeader('Authorization', 'Basic ' + btoa(this.clientId + ':' + this.clientSecret));
	            req.withCredentials = true;

	            req.onreadystatechange = function () {
	                if (req.readyState == 4) {
	                    if (req.status == 200) {
	                        deferred.resolve(JSON.parse(req.responseText));
	                    } else {
	                        deferred.reject(req.responseText === '' ? '' : JSON.parse(req.responseText));
	                    }
	                }
	            };
	            req.send(JSON.stringify(user));

	            return deferred.promise;
	        }
	    }, {
	        key: "decodeToken",
	        value: function decodeToken(str) {
	            str = str.split('.')[1];
	            str = str.replace('/-/g', '+');
	            str = str.replace('/_/g', '/');
	            switch (str.length % 4) {
	                case 0:
	                    break;
	                case 2:
	                    str += '==';
	                    break;
	                case 3:
	                    str += '=';
	                    break;
	                default:
	                    throw 'Invalid token';
	            }

	            str = (str + '===').slice(0, str.length + str.length % 4);
	            str = str.replace(/-/g, '+').replace(/_/g, '/');

	            str = decodeURIComponent(escape(atob(str)));

	            str = JSON.parse(str);
	            return str;
	        }
	    }, {
	        key: "login",
	        value: function login(username, password, afterRegister) {
	            var _this3 = this;

	            var deferred = this.$q.defer();

	            var req = new XMLHttpRequest();
	            req.open('POST', this.urls.token, true);
	            req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	            req.setRequestHeader('Authorization', 'Basic ' + btoa(this.clientId + ':' + this.clientSecret));
	            req.withCredentials = true;

	            req.onreadystatechange = function () {
	                if (req.readyState == 4) {
	                    if (req.status == 200) {
	                        var data = JSON.parse(req.responseText);
	                        _this3.onAuthSuccess(data.access_token, data.refresh_token, afterRegister);
	                        deferred.resolve();
	                    } else {
	                        deferred.reject(req.responseText === '' ? '' : JSON.parse(req.responseText));
	                    }
	                }
	            };
	            req.send('grant_type=password&username=' + encodeURIComponent(username) + '&password=' + encodeURIComponent(password));
	            return deferred.promise;
	        }
	    }, {
	        key: "forgotPassword",
	        value: function forgotPassword(email) {
	            var deferred = this.$q.defer();

	            var req = new XMLHttpRequest();
	            req.open('POST', this.urls.forgotPassword, true);
	            req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	            req.setRequestHeader('Authorization', 'bearer ' + this.token);
	            req.withCredentials = true;

	            req.onreadystatechange = function () {
	                if (req.readyState == 4) {
	                    if (req.status == 200) {
	                        var data = JSON.parse(req.responseText);
	                        deferred.resolve();
	                    } else {
	                        deferred.reject(req.responseText === '' ? '' : JSON.parse(req.responseText));
	                    }
	                }
	            };
	            req.send('email=' + encodeURIComponent(email));
	            return deferred.promise;
	        }
	    }, {
	        key: "isTokenExpired",
	        value: function isTokenExpired(minValidity, exp, timeSkew) {
	            if (minValidity === -1) {
	                return true;
	            }
	            var expiresIn = exp - Math.ceil(new Date().getTime() / 1000) + timeSkew;
	            if (minValidity) {
	                expiresIn -= minValidity;
	            }
	            return expiresIn < 0;
	        }
	    }, {
	        key: "refreshToken",
	        value: function refreshToken(minValidity, exp, timeSkew, _refreshToken) {
	            var _this4 = this;

	            var deferred = this.$q.defer();

	            //If the user is not authenticated
	            if (_refreshToken === '') {
	                deferred.reject();
	                return deferred.promise;
	            }
	            //If the access token is still valid
	            if (!this.isTokenExpired(minValidity, exp, timeSkew)) {
	                deferred.resolve();
	                return deferred.promise;
	            }

	            var req = new XMLHttpRequest();
	            req.open('POST', this.urls.token, true);
	            req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	            req.setRequestHeader('Authorization', 'Basic ' + btoa(this.clientId + ':' + this.clientSecret));
	            req.withCredentials = true;
	            req.timeout = 2000;

	            req.onreadystatechange = function () {
	                if (req.readyState == 4) {
	                    if (req.status == 200) {
	                        var data = JSON.parse(req.responseText);
	                        _this4.onAuthSuccess(data.access_token, data.refresh_token);
	                        deferred.resolve();
	                    } else {
	                        if (req.status >= 400 && req.status < 500) {
	                            _this4.logout();
	                        }
	                        if (minValidity === -1 && req.status < 400) {
	                            _this4.refreshToken(3600, exp, timeSkew, _refreshToken).then(function (data) {
	                                deferred.resolve();
	                            }, function () {
	                                deferred.reject();
	                            });
	                        } else {
	                            deferred.reject(req.responseText === '' ? '' : JSON.parse(req.responseText));
	                        }
	                    }
	                }
	            };
	            req.send('grant_type=refresh_token&refresh_token=' + encodeURIComponent(_refreshToken));
	            return deferred.promise;
	        }
	    }, {
	        key: "onAuthSuccess",
	        value: function onAuthSuccess(token, refreshToken) {
	            var afterRegister = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

	            var broadcastSignal = !this.authenticated ? true : false;
	            this.authenticated = true;
	            this.kcToken = token;
	            this.kcRefreshToken = refreshToken;
	            this.kcTokenParsed = this.decodeToken(token);
	            this.timeSkew = Math.floor(new Date().getTime() / 1000) - this.kcTokenParsed.iat;

	            this.userInfo.email = this.kcTokenParsed.email;
	            this.userInfo.pseudo = this.kcTokenParsed.preferred_username;
	            this.userInfo.firstName = this.kcTokenParsed.given_name;
	            this.userInfo.lastName = this.kcTokenParsed.family_name;
	            this.userInfo.id = this.kcTokenParsed.sub;

	            window.localStorage.setItem('keycloakToken', this.kcToken);
	            window.localStorage.setItem('keycloakRefreshToken', this.kcRefreshToken);

	            if (broadcastSignal) {
	                if (this.termsConditions !== null) {
	                    this.checkTermsConditionsVersion(afterRegister);
	                } else {
	                    this.showFirstState(afterRegister);
	                }
	            }
	        }
	    }, {
	        key: "checkTermsConditionsVersion",
	        value: function checkTermsConditionsVersion() {
	            var _this5 = this;

	            var afterRegister = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

	            var req = new XMLHttpRequest();
	            req.open('GET', this.urls.termsConditionsVersion, true);

	            req.onreadystatechange = function () {
	                if (req.readyState == 4) {
	                    if (req.status == 200) {
	                        var localVersion = window.localStorage.getItem("keycloakTermsConditionsValidatedVersion-" + _this5.userInfo.id);
	                        var data = JSON.parse(req.responseText);
	                        if (data[_this5.termsConditions.versionField] != localVersion) {
	                            _this5.termsConditions.newVersion = data[_this5.termsConditions.versionField];
	                            _this5.termsConditions.afterRegister = afterRegister;
	                            _this5.go('keycloakTermsConditions');
	                            return;
	                        }
	                    }
	                    _this5.showFirstState(afterRegister);
	                }
	            };
	            req.send();
	        }
	    }, {
	        key: "showFirstState",
	        value: function showFirstState(afterRegister) {
	            this.$rootScope.$broadcast("keycloakAuthenticated");
	            var state = '';
	            if (afterRegister) {
	                state = angular.isFunction(this.afterRegisterState) ? this.afterRegisterState() : this.afterRegisterState;
	            } else {
	                state = angular.isFunction(this.afterLoginState) ? this.afterLoginState() : this.afterLoginState;
	            }
	            this.go(state);
	        }
	    }, {
	        key: "go",
	        value: function go(state) {
	            var _this6 = this;

	            this.$timeout(function () {
	                _this6.$ionicHistory.clearHistory();
	                _this6.$ionicHistory.nextViewOptions({ historyRoot: true, disableBack: true });
	                _this6.$state.go(state);
	            });
	        }
	    }]);

	    return KeycloakMobileService;
	}();

	exports.KeycloakMobileService = KeycloakMobileService;

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.KeycloakServiceProvider = undefined;

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _keycloakInterceptor = __webpack_require__(5);

	var _keycloakRBAC = __webpack_require__(6);

	var _keycloakHasAccess = __webpack_require__(7);

	var _keycloakApp = __webpack_require__(8);

	var _keycloakMobile = __webpack_require__(3);

	var _keycloakDefault = __webpack_require__(2);

	var _login = __webpack_require__(9);

	var _register = __webpack_require__(10);

	var _forgotPassword = __webpack_require__(1);

	var _termsConditions = __webpack_require__(11);

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var KeycloakServiceProvider = exports.KeycloakServiceProvider = function () {
	    KeycloakServiceProvider.$inject = ["$stateProvider"];
	    function KeycloakServiceProvider($stateProvider) {
	        'ngInject';

	        _classCallCheck(this, KeycloakServiceProvider);

	        this.socialNetworks = null;
	        this.mobileMode = false;
	        this.rememberMe = false;
	        this.termsConditions = null;
	        this.$stateProvider = $stateProvider;
	    }

	    _createClass(KeycloakServiceProvider, [{
	        key: 'activeRememberMe',
	        value: function activeRememberMe() {
	            this.rememberMe = true;
	        }
	    }, {
	        key: 'activeEmailHint',
	        value: function activeEmailHint() {
	            this.addEmailHint = true;
	        }
	    }, {
	        key: 'activeTermsConditionsView',
	        value: function activeTermsConditionsView(dataField, versionField) {
	            this.termsConditions = {
	                dataField: dataField,
	                versionField: versionField
	            };
	        }
	    }, {
	        key: 'activeFacebook',
	        value: function activeFacebook() {
	            if (this.socialNetworks === null) {
	                this.socialNetworks = {};
	            }
	            this.socialNetworks.facebook = {
	                key: 'facebook',
	                image: 'assets/images/keycloak/facebook.png'
	            };
	        }
	    }, {
	        key: 'activeGoogle',
	        value: function activeGoogle() {
	            if (this.socialNetworks === null) {
	                this.socialNetworks = {};
	            }
	            this.socialNetworks.google = {
	                key: 'google',
	                image: 'assets/images/keycloak/google.png'
	            };
	        }
	    }, {
	        key: 'activeTwitter',
	        value: function activeTwitter() {
	            if (this.socialNetworks === null) {
	                this.socialNetworks = {};
	            }
	            this.socialNetworks.twitter = {
	                key: 'twitter',
	                image: 'assets/images/keycloak/twitter.png'
	            };
	        }
	    }, {
	        key: 'activeLinkedin',
	        value: function activeLinkedin() {
	            if (this.socialNetworks === null) {
	                this.socialNetworks = {};
	            }
	            this.socialNetworks.linkedin = {
	                key: 'linkedin',
	                image: 'assets/images/keycloak/linkedin.png'
	            };
	        }
	    }, {
	        key: 'setUrls',
	        value: function setUrls(urls) {
	            this.urls = urls;
	        }
	    }, {
	        key: 'setKeycloakUrl',
	        value: function setKeycloakUrl(url) {
	            this.keycloakUrl = url.replace(/\/$/, '');
	        }
	    }, {
	        key: 'setClientId',
	        value: function setClientId(clientId) {
	            this.clientId = clientId;
	        }
	    }, {
	        key: 'setClientSecret',
	        value: function setClientSecret(clientSecret) {
	            this.clientSecret = clientSecret;
	        }
	    }, {
	        key: 'setAfterLoginState',
	        value: function setAfterLoginState(afterLoginState) {
	            this.afterLoginState = afterLoginState;
	        }
	    }, {
	        key: 'setAfterRegisterState',
	        value: function setAfterRegisterState(afterRegisterState) {
	            this.afterRegisterState = afterRegisterState;
	        }
	    }, {
	        key: 'setAfterLogoutState',
	        value: function setAfterLogoutState(afterLogoutState) {
	            this.afterLogoutState = afterLogoutState;
	        }
	    }, {
	        key: 'setMobileMode',
	        value: function setMobileMode(mobileMode) {
	            this.mobileMode = mobileMode;
	            if (mobileMode) {
	                this.$stateProvider.state('keycloakLogin', {
	                    url: '/keycloak/login',
	                    template: '<ion-view cache-view="false" can-swipe-back="false" class="keycloak-view login-view"><div class="col alert-wrapper"><div class="alert alert-error" ng-if="ctrl.error && ctrl.loginForm.$pristine"><i class="ion-alert-circled"></i> <span class="feedback-text" translate>keycloak.login.wrongCredentials</span></div><div class="alert alert-success" ng-if="ctrl.keycloakService.loginMessage && ctrl.loginForm.$pristine"><i class="ion-checkmark-circled"></i> <span class="feedback-text" translate>keycloak.login.emailSent</span></div></div><ion-content><div class="content-wrapper"><div class="keycloak-logo"><img src="assets/images/keycloak/logo.png"></div><form class="login-form" name="ctrl.loginForm" ng-submit="ctrl.login(ctrl.username, ctrl.password)"><div class="form-group"><div class="form-icon"><img src="assets/images/keycloak/email.svg"></div><div class="form-label"><label for="username" class="control-label" translate>keycloak.login.username</label></div><div class="form-input"><input id="username" class="form-control" ng-model="ctrl.username" name="username" type="text" required autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"></div></div><div class="form-group"><div class="form-icon"><img src="assets/images/keycloak/password.svg"></div><div class="form-label"><label for="password" class="control-label" translate>keycloak.login.password</label></div><div class="form-input"><input id="password" ng-model="ctrl.password" class="form-control" name="password" type="password" autocomplete="off" required autocorrect="off" autocapitalize="off" spellcheck="false"></div></div><div class="form-group remember-me-wrapper" ng-if="ctrl.keycloakService.rememberMe"><ion-toggle class="remember-me-toogle" ng-model="ctrl.rememberMe" toggle-class="toggle-dark">{{\'keycloak.login.rememberMe\' | translate}}</ion-toggle></div><div class="row form-buttons"><button class="keycloak-button-block button button-block button-dark" type="submit" ng-disabled="ctrl.loginForm.$invalid" translate>keycloak.login.login</button></div></form><div class="social-network-container text-center" ng-if="ctrl.keycloakService.socialNetworks !== null"><span translate>keycloak.login.loginWith</span><br><a class="facebook-btn social-network-btn" type="button" ng-click="ctrl.loginWithFacebook()" ng-if="ctrl.keycloakService.socialNetworks.facebook !== undefined"><img height="35" ng-src="{{ctrl.keycloakService.socialNetworks.facebook.image}}"></a> <a class="google-btn social-network-btn" type="button" ng-click="ctrl.loginWithGoogle()" ng-if="ctrl.keycloakService.socialNetworks.google !== undefined"><img height="35" ng-src="{{ctrl.keycloakService.socialNetworks.google.image}}"></a> <a class="twitter-btn social-network-btn" type="button" ng-click="ctrl.loginWithTwitter()" ng-if="ctrl.keycloakService.socialNetworks.twitter !== undefined"><img height="35" ng-src="{{ctrl.keycloakService.socialNetworks.twitter.image}}"></a> <a class="linkedin-btn social-network-btn" type="button" ng-click="ctrl.loginWithLinkedin()" ng-if="ctrl.keycloakService.socialNetworks.linkedin !== undefined"><img height="35" ng-src="{{ctrl.keycloakService.socialNetworks.linkedin.image}}"></a></div><div class="info-container"><span><a ui-sref="keycloakForgotPassword" translate>keycloak.login.forgot</a></span><br><br><span translate>keycloak.login.noAccount</span> <a ui-sref="keycloakRegister" translate>keycloak.login.signup</a></div></div></ion-content></ion-view>',
	                    controller: 'LoginController',
	                    controllerAs: 'ctrl'
	                }).state('keycloakRegister', {
	                    url: '/keycloak/register',
	                    template: '<ion-view cache-view="false" can-swipe-back="false" class="keycloak-view register-view"><div class="alert alert-error" ng-if="ctrl.error && ctrl.registerForm.$pristine"><i class="ion-alert-circled"></i> <span ng-if="ctrl.errorType === \'keycloak.conflict\'" class="feedback-text" translate>keycloak.invalidEmail</span> <span ng-if="ctrl.errorType === \'wrongConfPassword\'" class="feedback-text" translate>keycloak.invalidConfPassword</span> <span ng-if="ctrl.errorType !== \'wrongConfPassword\' && ctrl.errorType !== \'keycloak.conflict\'" class="feedback-text">{{\'keycloak.\' + ctrl.errorType | translate}}</span></div><ion-content><div class="content-wrapper"><form class="register-form" name="ctrl.registerForm" ng-submit="ctrl.register()"><div class="form-group"><div class="form-icon"><img src="assets/images/keycloak/firstName.svg"></div><div class="form-label"><label for="firstName" class="control-label" translate>keycloak.register.firstName</label></div><div class="form-input"><input id="firstName" class="form-control" ng-model="ctrl.user.firstName" name="firstName" type="text" required></div></div><div class="form-group"><div class="form-icon"><img src="assets/images/keycloak/lastName.svg"></div><div class="form-label"><label for="lastName" class="control-label" translate>keycloak.register.lastName</label></div><div class="form-input"><input id="lastName" class="form-control" ng-model="ctrl.user.lastName" name="lastName" type="text" required></div></div><div class="form-group" ng-class="{\'email-with-hint\' : ctrl.keycloakService.addEmailHint}"><div class="form-icon"><img src="assets/images/keycloak/email.svg"></div><div class="form-label"><label for="email" class="control-label" translate>keycloak.register.email</label></div><div class="form-input"><input id="email" class="form-control" ng-model="ctrl.user.email" name="email" type="email" required autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"></div></div><div class="form-label" ng-if="ctrl.keycloakService.addEmailHint"><label for="password" class="control-label email-hint" translate>keycloak.register.emailHint</label></div><div class="form-group username-field"><div class="form-icon"><img src="assets/images/keycloak/username.svg"></div><div class="form-label"><label for="username" class="control-label" translate>keycloak.register.username</label></div><div class="form-input"><input id="username" class="form-control" ng-model="ctrl.user.pseudo" name="username" type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"></div></div><div class="form-group password-with-hint"><div class="form-icon"><img src="assets/images/keycloak/password.svg"></div><div class="form-label"><label for="password" class="control-label" translate>keycloak.register.password</label></div><div class="form-input"><input id="password" ng-model="ctrl.user.password" class="form-control" name="password" type="password" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" required></div></div><div class="form-label"><label for="password" class="control-label password-hint" translate>keycloak.register.passwordHint</label></div><div class="form-group password-with-hint"><div class="form-icon"><img src="assets/images/keycloak/password.svg"></div><div class="form-label"><label for="confirmPassword" class="control-label" translate>keycloak.register.confirmPassword</label></div><div class="form-input"><input id="confirmPassword" ng-model="ctrl.confirmPassword" class="form-control" name="confirmPassword" type="password" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" required></div></div><div class="form-label" ng-if="(ctrl.confirmPassword !== ctrl.user.password && ctrl.confirmPassword !== \'\')"><label for="password" class="control-label password-hint" translate>keycloak.invalidConfPassword</label></div><div class="row form-buttons"><button class="keycloak-button-block button button-block button-dark" type="submit" ng-disabled="ctrl.registerForm.$invalid || (ctrl.confirmPassword !== ctrl.user.password && ctrl.confirmPassword !== \'\')" translate>keycloak.register.register</button></div></form><div class="info-container"><a ui-sref="keycloakLogin" translate>keycloak.register.back</a></div></div></ion-content></ion-view>',
	                    controller: 'RegisterController',
	                    controllerAs: 'ctrl'
	                }).state('keycloakForgotPassword', {
	                    url: '/keycloak/forgotPassword',
	                    template: '<ion-view cache-view="false" can-swipe-back="false" class="keycloak-view forgot-password-view"><div class="col alert-wrapper"><div class="alert alert-error" ng-if="ctrl.error && ctrl.forgotForm.$pristine"><i class="ion-alert-circled"></i> <span class="feedback-text" translate>keycloak.forgotPassword.error</span></div></div><ion-content><div class="content-wrapper"><div class="keycloak-logo"><img src="assets/images/keycloak/logo.png"></div><form class="login-form" name="ctrl.forgotForm" ng-submit="ctrl.forgotPassword()"><div class="form-group"><div class="form-icon"><img src="assets/images/keycloak/email.svg"></div><div class="form-label"><label for="email" class="control-label" translate>keycloak.forgotPassword.email</label></div><div class="form-input"><input id="email" class="form-control" ng-model="ctrl.email" name="email" type="text" required autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"></div></div><div class="row form-buttons"><button class="keycloak-button-block button button-block button-dark" type="submit" ng-disabled="ctrl.forgotForm.$invalid" translate>keycloak.forgotPassword.submit</button></div></form><div class="info-container"><span translate>keycloak.forgotPassword.info</span><br><br><a ui-sref="keycloakLogin" translate>keycloak.forgotPassword.back</a></div></div></ion-content></ion-view>',
	                    controller: 'ForgotPasswordController',
	                    controllerAs: 'ctrl'
	                }).state('keycloakTermsConditions', {
	                    url: '/keycloak/termsConditions',
	                    template: '<ion-view cache-view="false" can-swipe-back="false" class="keycloak-view forgot-password-view"><div class="col alert-wrapper"><div class="alert alert-error" ng-if="ctrl.error && ctrl.forgotForm.$pristine"><i class="ion-alert-circled"></i> <span class="feedback-text" translate>keycloak.forgotPassword.error</span></div></div><ion-content><div class="content-wrapper"><div class="keycloak-logo"><img src="assets/images/keycloak/logo.png"></div><div class="text-justify terms-conditions" ng-bind-html="ctrl.termsConditions"></div></div></ion-content><ion-footer-bar class="action-footer"><button class="button button-dark pull-left" ng-click="ctrl.decline()" translate>keycloak.termsConditions.decline</button> <button class="button button-dark item-center" ng-click="ctrl.accept()" translate>keycloak.termsConditions.accept</button></ion-footer-bar></ion-view>',
	                    controller: 'TermsConditionsController',
	                    controllerAs: 'ctrl'
	                }).state('menu.keycloakTermsConditions', {
	                    url: '/keycloak/termsConditionsWithMenu',
	                    views: {
	                        'menuContent': {
	                            template: '<ion-view view-title="{{\'keycloak.termsConditions.termsConditions\' | translate}}" class="keycloak-view forgot-password-view"><ion-content><div class="content-wrapper"><div class="keycloak-logo"><img src="assets/images/keycloak/logo.png"></div><div class="text-justify terms-conditions" ng-bind-html="ctrl.termsConditions"></div></div></ion-content></ion-view>',
	                            controller: 'TermsConditionsController',
	                            controllerAs: 'ctrl'
	                        }
	                    }
	                });
	            }
	        }
	    }, {
	        key: '$get',
	        value: ["$http", "$q", "$rootScope", "$state", "$timeout", "$injector", function $get($http, $q, $rootScope, $state, $timeout, $injector) {
	            'ngInject';

	            if (this.mobileMode) {
	                return new _keycloakMobile.KeycloakMobileService($http, $q, $rootScope, $state, $timeout, $injector.get('$ionicHistory'), this.urls, this.clientId, this.clientSecret, this.afterLoginState, this.afterRegisterState, this.afterLogoutState, this.termsConditions, this.rememberMe, this.addEmailHint, this.socialNetworks);
	            }
	            return new _keycloakDefault.KeycloakDefaultService($q, $rootScope, this.urls);
	        }]
	    }]);

	    return KeycloakServiceProvider;
	}();

	angular.module('rdl.keycloak', []).directive('rdlHasAccess', _keycloakHasAccess.keycloakHasAccess).directive('rdlKeycloakApp', _keycloakApp.keycloakApp).provider('keycloakService', KeycloakServiceProvider).provider('keycloakRBACService', _keycloakRBAC.KeycloakRBACServiceProvider).provider('keycloakInterceptor', _keycloakInterceptor.KeycloakInterceptorProvider).controller('LoginController', _login.LoginController).controller('RegisterController', _register.RegisterController).controller('ForgotPasswordController', _forgotPassword.ForgotPasswordController).controller('TermsConditionsController', _termsConditions.TermsConditionsController);

/***/ },
/* 5 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var KeycloakInterceptor = exports.KeycloakInterceptor = function () {
	    function KeycloakInterceptor($q, $injector, backendUrl) {
	        _classCallCheck(this, KeycloakInterceptor);

	        this.$q = $q;
	        this.backendUrl = backendUrl;
	        this.$injector = $injector;
	        this.keycloakService = null;

	        return {
	            request: this.request.bind(this)
	        };
	    }

	    _createClass(KeycloakInterceptor, [{
	        key: 'request',
	        value: function request(config) {
	            if (this.keycloakService === null) {
	                //To avoid circular dependancies
	                this.keycloakService = this.$injector.get('keycloakService');
	            }
	            if (config.url.indexOf(this.backendUrl) > -1) {
	                angular.extend(config.headers, this.keycloakService.getHeaders());
	            }

	            return config;
	        }
	    }], [{
	        key: 'create',
	        value: function create($q, $injector) {
	            return new KeycloakInterceptor($q, $injector);
	        }
	    }]);

	    return KeycloakInterceptor;
	}();

	var KeycloakInterceptorProvider = exports.KeycloakInterceptorProvider = function () {
	    KeycloakInterceptorProvider.$inject = ["$httpProvider"];
	    function KeycloakInterceptorProvider($httpProvider) {
	        'ngInject';

	        _classCallCheck(this, KeycloakInterceptorProvider);

	        this.$httpProvider = $httpProvider;
	        this.urls = [];
	    }

	    _createClass(KeycloakInterceptorProvider, [{
	        key: 'setBackendUrl',
	        value: function setBackendUrl(url) {
	            this.backendUrl = url;
	        }
	    }, {
	        key: 'install',
	        value: function install() {
	            this.$httpProvider.interceptors.push('keycloakInterceptor');
	        }
	    }, {
	        key: '$get',
	        value: ["$q", "$injector", function $get($q, $injector) {
	            'ngInject';

	            return new KeycloakInterceptor($q, $injector, this.backendUrl);
	        }]
	    }]);

	    return KeycloakInterceptorProvider;
	}();

/***/ },
/* 6 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var KeycloakRBACService = function () {
	    function KeycloakRBACService($injector, $state, $rootScope, $timeout, deniedState, resourcesAccess) {
	        _classCallCheck(this, KeycloakRBACService);

	        if (this.keycloakService === null) {
	            //To avoid circular dependancies
	            this.keycloakService = this.$injector.get('keycloakService');
	        }
	        this.keycloakService = null;
	        this.resourcesAccess = resourcesAccess;
	        this.$injector = $injector;
	        this.$state = $state;
	        this.$timeout = $timeout;
	        this.$rootScope = $rootScope;
	        this.deniedState = deniedState;
	    }

	    _createClass(KeycloakRBACService, [{
	        key: 'start',
	        value: function start() {
	            var _this = this;

	            this.$rootScope.$on('$stateChangeStart', function (event, toState) {
	                try {
	                    _this.checkAccess(toState.access);
	                } catch (err) {
	                    console.log("Error during the role checking");
	                    _this.$state.go(_this.deniedState);
	                }
	            });
	        }
	    }, {
	        key: 'checkAccess',
	        value: function checkAccess(access) {
	            var _this2 = this;

	            if (this.keycloakService === null) {
	                //To avoid circular dependancies
	                this.keycloakService = this.$injector.get('keycloakService');
	            }

	            //Public access
	            if (angular.isUndefined(access) || angular.isUndefined(access.requiredLogin) || !access.requiredLogin) {
	                return;
	            }
	            //Authenticated but no required role
	            if (this.keycloakService.isAuthenticated() && angular.isUndefined(access.requiredRole)) {
	                return;
	            }

	            var roles = access.requiredRole.split('|');
	            for (var i = 0; i < roles.length; ++i) {
	                var role = roles[i].replace(/{(.*)}/, function (match, p1) {
	                    return _this2.keycloakService.tokenParsed()[p1];
	                });

	                //Realm role access
	                if (this.keycloakService.hasRealmRole(role)) {
	                    return;
	                }

	                //Client role access
	                if (this.keycloakService.hasResourceRole(role)) {
	                    return;
	                }
	            };

	            //If the user is authenticate, we go to the deniedState
	            if (this.keycloakService.isAuthenticated()) {
	                this.$timeout(function () {
	                    _this2.$state.go(_this2.deniedState);
	                });
	            } else {
	                this.keycloakService.authenticate();
	            }
	        }
	    }, {
	        key: 'hasAccessToResource',
	        value: function hasAccessToResource(resource) {
	            if (this.keycloakService === null) {
	                //To avoid circular dependancies
	                this.keycloakService = this.$injector.get('keycloakService');
	            }

	            var acceptedRoles = this.resourcesAccess[resource];
	            if (angular.isUndefined(acceptedRoles)) {
	                return false;
	            }

	            for (var i = 0; i < acceptedRoles.length; ++i) {
	                if (this.keycloakService.hasRealmRole(acceptedRoles[i])) {
	                    return true;
	                }
	            }
	            return false;
	        }
	    }]);

	    return KeycloakRBACService;
	}();

	var KeycloakRBACServiceProvider = exports.KeycloakRBACServiceProvider = function () {
	    function KeycloakRBACServiceProvider() {
	        _classCallCheck(this, KeycloakRBACServiceProvider);

	        this.deniedState = '';
	        this.resourcesAccess = {};
	    }

	    _createClass(KeycloakRBACServiceProvider, [{
	        key: 'setDeniedState',
	        value: function setDeniedState(deniedState) {
	            this.deniedState = deniedState;
	        }
	    }, {
	        key: 'setResourcesAccess',
	        value: function setResourcesAccess(resourcesAccess) {
	            this.resourcesAccess = resourcesAccess;
	        }
	    }, {
	        key: '$get',
	        value: ["$injector", "$state", "$rootScope", "$timeout", function $get($injector, $state, $rootScope, $timeout) {
	            'ngInject';

	            return new KeycloakRBACService($injector, $state, $rootScope, $timeout, this.deniedState, this.resourcesAccess);
	        }]
	    }]);

	    return KeycloakRBACServiceProvider;
	}();

/***/ },
/* 7 */
/***/ function(module, exports) {

	'use strict';

	keycloakHasAccess.$inject = ["ngIfDirective", "keycloakRBACService"];
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.keycloakHasAccess = keycloakHasAccess;
	function keycloakHasAccess(ngIfDirective, keycloakRBACService) {
	    'ngInject';

	    var ngIf = ngIfDirective[0];

	    return {
	        transclude: ngIf.transclude,
	        priority: ngIf.priority,
	        terminal: ngIf.terminal,
	        restrict: ngIf.restrict,
	        link: function link($scope, $element, $attr) {
	            var hasAccess = keycloakRBACService.hasAccessToResource($attr.rdlHasAccess);
	            $attr.ngIf = function () {
	                return hasAccess;
	            };
	            ngIf.link.apply(ngIf, arguments);
	        }
	    };
	};

/***/ },
/* 8 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.keycloakApp = keycloakApp;
	function keycloakApp(keycloakService, $compile) {
	    return {
	        terminal: true,
	        compile: function compile(element, attrs) {
	            var contents = element.contents();
	            element.empty();
	            return function postLink(scope, element) {
	                keycloakService.startPromise.promise.then(function () {
	                    element.append($compile(contents)(scope));
	                });
	            };
	        }
	    };
	};

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var LoginController = exports.LoginController = function () {
	    LoginController.$inject = ["keycloakService", "$ionicLoading", "$filter", "$scope"];
	    function LoginController(keycloakService, $ionicLoading, $filter, $scope) {
	        'ngInject';

	        var _this = this;

	        _classCallCheck(this, LoginController);

	        this.keycloakService = keycloakService;
	        this.$ionicLoading = $ionicLoading;
	        this.$filter = $filter;
	        this.error = false;
	        this.rememberMe = true;
	        $scope.$on('$ionicView.enter', function () {
	            return _this.init();
	        });
	    }

	    _createClass(LoginController, [{
	        key: 'init',
	        value: function init() {
	            this.loginForm.$setPristine();
	            if (this.keycloakService.rememberMe) {
	                this.username = window.localStorage.getItem("keycloakUsername");
	                this.password = window.localStorage.getItem("keycloakPassword");
	            }
	        }
	    }, {
	        key: 'login',
	        value: function login(username, password) {
	            var _this2 = this;

	            var waiting = this.$filter('translate')('keycloak.waiting');
	            this.$ionicLoading.show({ template: waiting });
	            this.keycloakService.login(username, password, false).then(function () {
	                _this2.$ionicLoading.hide();
	                if (_this2.rememberMe && _this2.keycloakService.rememberMe) {
	                    window.localStorage.setItem("keycloakUsername", username);
	                    window.localStorage.setItem("keycloakPassword", password);
	                } else {
	                    window.localStorage.removeItem("keycloakUsername");
	                    window.localStorage.removeItem("keycloakPassword");
	                }
	            }, function (error) {
	                _this2.loginForm.$setPristine();
	                _this2.$ionicLoading.hide();
	                _this2.error = true;
	            });
	        }
	    }, {
	        key: 'loginWithFacebook',
	        value: function loginWithFacebook() {
	            var _this3 = this;

	            facebookConnectPlugin.login(['public_profile', 'email'], function (res) {
	                var waiting = _this3.$filter('translate')('keycloak.waiting');
	                _this3.$ionicLoading.show({ template: waiting });
	                facebookConnectPlugin.api("/" + res.authResponse.userID + "?fields=id,email,currency,last_name,first_name,gender", ['public_profile', 'email'], function (info) {
	                    var loginInfo = {
	                        token: res.authResponse.accessToken,
	                        id: res.authResponse.userID,
	                        email: info.email,
	                        pseudo: info.email,
	                        icon: "https://graph.facebook.com/" + res.authResponse.userID + "/picture?type=large",
	                        currency: info.currency.user_currency,
	                        lastName: info.last_name,
	                        firstName: info.first_name,
	                        gender: info.gender
	                    };
	                    _this3.checkSocialNetworkLogin(_this3.keycloakService.socialNetworks.facebook, loginInfo);
	                });
	            }, function (e) {
	                console.log(e);
	            });
	        }
	    }, {
	        key: 'loginWithLinkedin',
	        value: function loginWithLinkedin() {
	            var _this4 = this;

	            cordova.plugins.LinkedIn.login(['r_basicprofile', 'r_emailaddress'], true, function () {
	                cordova.plugins.LinkedIn.getActiveSession(function (token) {
	                    cordova.plugins.LinkedIn.getRequest('people/~:(id,first-name,last-name,email-address,picture-urls::(original))', function (info) {
	                        var loginInfo = {
	                            token: token.accessToken,
	                            id: info.id,
	                            email: info.emailAddress,
	                            pseudo: info.emailAddress,
	                            icon: info.pictureUrls.values.length > 0 ? info.pictureUrls.values[0] : '',
	                            lastName: info.lastName,
	                            firstName: info.firstName
	                        };
	                        _this4.checkSocialNetworkLogin(_this4.keycloakService.socialNetworks.linkedin, loginInfo);
	                    }, function (e) {
	                        console.log(e);
	                    });
	                }, function (e) {
	                    console.log(e);
	                });
	            }, function (e) {
	                console.log(e);
	            });
	        }
	    }, {
	        key: 'loginWithGoogle',
	        value: function loginWithGoogle(socialNetwork) {
	            var _this5 = this;

	            window.plugins.googleplus.login({ scopes: 'profile email' }, function (res) {
	                if (angular.isUndefined(res.familyName)) {
	                    res.givenName = res.displayName.substr(0, res.displayName.indexOf(' '));
	                    res.familyName = res.displayName.substr(res.displayName.indexOf(' ') + 1);
	                }
	                var loginInfo = {
	                    token: res.accessToken,
	                    id: res.userId,
	                    email: res.email,
	                    pseudo: res.email,
	                    icon: res.imageUrl,
	                    lastName: res.familyName,
	                    firstName: res.givenName
	                };
	                _this5.checkSocialNetworkLogin(_this5.keycloakService.socialNetworks.google, loginInfo);
	            }, function (e) {
	                console.log(e);
	            });
	        }
	    }, {
	        key: 'loginWithTwitter',
	        value: function loginWithTwitter(socialNetwork) {
	            var _this6 = this;

	            TwitterConnect.login(function (res) {
	                console.log(res);
	                var req = new XMLHttpRequest();
	                req.open('POST', _this6.keycloakService.urls.twitterInfo, true);
	                req.setRequestHeader('Client-Id', _this6.keycloakService.clientId);
	                req.setRequestHeader('Content-type', 'application/json');
	                req.withCredentials = true;

	                req.onreadystatechange = function () {
	                    if (req.readyState == 4) {
	                        if (req.status == 200) {
	                            var twitterInfo = JSON.parse(req.responseText);
	                            var loginInfo = {
	                                token: res.token,
	                                id: res.userId,
	                                email: twitterInfo.email,
	                                pseudo: twitterInfo.email,
	                                icon: twitterInfo.icon,
	                                lastName: twitterInfo.name.substr(twitterInfo.name.indexOf(' ') + 1),
	                                firstName: twitterInfo.name.substr(0, twitterInfo.name.indexOf(' '))
	                            };
	                            _this6.checkSocialNetworkLogin(_this6.keycloakService.socialNetworks.twitter, loginInfo);
	                        } else {
	                            console.log( true ? '' : JSON.parse(req.responseText));
	                        }
	                    }
	                };
	                req.send(JSON.stringify({
	                    token: res.token,
	                    secret: res.secret
	                }));
	            }, function (e) {
	                console.log(e);
	            });
	        }
	    }, {
	        key: 'checkSocialNetworkLogin',
	        value: function checkSocialNetworkLogin(socialNetwork, loginInfo) {
	            var _this7 = this;

	            var req = new XMLHttpRequest();
	            req.open('POST', this.keycloakService.urls.socialLogin + "/" + socialNetwork.key, true);
	            req.setRequestHeader('Content-type', 'application/json');
	            req.setRequestHeader('Client-Id', this.keycloakService.clientId);
	            req.withCredentials = true;

	            req.onreadystatechange = function () {
	                if (req.readyState == 4) {
	                    if (req.status == 200) {
	                        _this7.login(loginInfo.email, loginInfo.token);
	                    } else {
	                        console.log( true ? '' : JSON.parse(req.responseText));
	                    }
	                }
	            };
	            req.send(JSON.stringify(loginInfo));
	        }
	    }]);

	    return LoginController;
	}();

/***/ },
/* 10 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var RegisterController = exports.RegisterController = function () {
	  RegisterController.$inject = ["keycloakService", "$ionicLoading", "$filter"];
	  function RegisterController(keycloakService, $ionicLoading, $filter) {
	    'ngInject';

	    _classCallCheck(this, RegisterController);

	    this.$ionicLoading = $ionicLoading;
	    this.$filter = $filter;
	    this.keycloakService = keycloakService;
	    this.confirmPassword = '';
	    this.user = {
	      firstName: "",
	      lastName: "",
	      email: "",
	      pseudo: "",
	      password: ""
	    };
	    this.error = false;
	  }

	  _createClass(RegisterController, [{
	    key: 'register',
	    value: function register() {
	      var _this = this;

	      if (this.user.password !== this.confirmPassword) {
	        this.error = true;
	        this.errorType = 'wrongConfPassword';
	        return;
	      }
	      var waiting = this.$filter('translate')('keycloak.waiting');
	      var user = angular.copy(this.user);
	      this.$ionicLoading.show({ template: waiting });
	      this.keycloakService.createUser(user).then(function () {
	        _this.keycloakService.login(_this.user.email, _this.user.password, true).then(function () {
	          _this.$ionicLoading.hide();
	        });
	      }, function (error) {
	        _this.registerForm.$setPristine();
	        _this.$ionicLoading.hide();
	        _this.error = true;
	        _this.errorType = error.error;
	      });
	    }
	  }]);

	  return RegisterController;
	}();

/***/ },
/* 11 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var TermsConditionsController = exports.TermsConditionsController = function () {
	    TermsConditionsController.$inject = ["$http", "keycloakService", "$scope"];
	    function TermsConditionsController($http, keycloakService, $scope) {
	        'ngInject';

	        var _this = this;

	        _classCallCheck(this, TermsConditionsController);

	        this.$http = $http;
	        this.keycloakService = keycloakService;
	        this.termsConditions = '';
	        $scope.$on('$ionicView.enter', function () {
	            return _this.loadConditions();
	        });
	    }

	    _createClass(TermsConditionsController, [{
	        key: 'loadConditions',
	        value: function loadConditions() {
	            var _this2 = this;

	            this.$http.get(this.keycloakService.urls.termsConditions).then(function (data) {
	                _this2.termsConditions = data.data[_this2.keycloakService.termsConditions.dataField];
	            });
	        }
	    }, {
	        key: 'accept',
	        value: function accept() {
	            window.localStorage.setItem('keycloakTermsConditionsValidatedVersion-' + this.keycloakService.userInfo.id, this.keycloakService.termsConditions.newVersion);
	            this.keycloakService.showFirstState(this.keycloakService.termsConditions.afterRegister);
	        }
	    }, {
	        key: 'decline',
	        value: function decline() {
	            this.keycloakService.logout();
	        }
	    }]);

	    return TermsConditionsController;
	}();

/***/ }
/******/ ]);