/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(11);


/***/ },

/***/ 11:
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var TermsConditionsController = exports.TermsConditionsController = function () {
	    TermsConditionsController.$inject = ["$http", "keycloakService", "$scope"];
	    function TermsConditionsController($http, keycloakService, $scope) {
	        'ngInject';

	        var _this = this;

	        _classCallCheck(this, TermsConditionsController);

	        this.$http = $http;
	        this.keycloakService = keycloakService;
	        this.termsConditions = '';
	        $scope.$on('$ionicView.enter', function () {
	            return _this.loadConditions();
	        });
	    }

	    _createClass(TermsConditionsController, [{
	        key: 'loadConditions',
	        value: function loadConditions() {
	            var _this2 = this;

	            this.$http.get(this.keycloakService.urls.termsConditions).then(function (data) {
	                _this2.termsConditions = data.data[_this2.keycloakService.termsConditions.dataField];
	            });
	        }
	    }, {
	        key: 'accept',
	        value: function accept() {
	            window.localStorage.setItem('keycloakTermsConditionsValidatedVersion-' + this.keycloakService.userInfo.id, this.keycloakService.termsConditions.newVersion);
	            this.keycloakService.showFirstState(this.keycloakService.termsConditions.afterRegister);
	        }
	    }, {
	        key: 'decline',
	        value: function decline() {
	            this.keycloakService.logout();
	        }
	    }]);

	    return TermsConditionsController;
	}();

/***/ }

/******/ });