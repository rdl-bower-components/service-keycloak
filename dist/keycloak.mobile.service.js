/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(3);


/***/ },
/* 1 */,
/* 2 */,
/* 3 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var KeycloakMobileService = function () {
	    function KeycloakMobileService($http, $q, $rootScope, $state, $timeout, $ionicHistory, urls, clientId, clientSecret, afterLoginState, afterRegisterState, afterLogoutState, termsConditions, rememberMe, addEmailHint, socialNetworks) {
	        _classCallCheck(this, KeycloakMobileService);

	        this.urls = urls;
	        this.clientId = clientId;
	        this.rememberMe = rememberMe;
	        this.addEmailHint = addEmailHint;
	        this.termsConditions = termsConditions;
	        this.clientSecret = clientSecret;
	        this.afterLoginState = afterLoginState;
	        this.afterRegisterState = afterRegisterState;
	        this.afterLogoutState = afterLogoutState;
	        this.$timeout = $timeout;
	        this.$http = $http;
	        this.$q = $q;
	        this.$rootScope = $rootScope;
	        this.$ionicHistory = $ionicHistory;
	        this.$state = $state;
	        this.socialNetworks = socialNetworks;
	        this.startPromise = this.$q.defer();

	        this.authenticated = false;

	        this.kcToken = "";
	        this.kcTokenParsed = {};
	        this.userInfo = {};
	        this.kcRefreshToken = "";
	    }

	    // ################## public ##################

	    _createClass(KeycloakMobileService, [{
	        key: "authenticate",
	        value: function authenticate() {
	            if (!this.authenticated) {
	                this.loginMessage = undefined;
	                this.$state.go("keycloakLogin");
	            }
	        }
	    }, {
	        key: "getHeaders",
	        value: function getHeaders() {
	            var addClientIdHeader = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	            var headers = {
	                "Accept": "application/json",
	                "Authorization": "bearer " + this.kcToken
	            };
	            if (addClientIdHeader) {
	                headers['Client-Id'] = this.clientId;
	            }
	            return headers;
	        }
	    }, {
	        key: "getToken",
	        value: function getToken() {
	            return this.kcToken;
	        }
	    }, {
	        key: "hasRealmRole",
	        value: function hasRealmRole(role) {
	            var access = this.kcTokenParsed.realm_access;
	            return !!access && access.roles.indexOf(role) >= 0;
	        }
	    }, {
	        key: "hasResourceRole",
	        value: function hasResourceRole(role, resource) {
	            var access = this.kcTokenParsed.resource_access;
	            if (!access) {
	                return false;
	            }
	            access = kc.resourceAccess[resource || this.clientId];
	            return !!access && access.roles.indexOf(role) >= 0;
	        }
	    }, {
	        key: "isAuthenticated",
	        value: function isAuthenticated() {
	            return this.authenticated;
	        }
	    }, {
	        key: "logout",
	        value: function logout() {
	            var _this = this;

	            var deferred = this.$q.defer();
	            var param = "refresh_token=" + encodeURIComponent(this.kcRefreshToken);
	            this.authenticated = false;
	            this.userInfo = {};
	            this.kcToken = "";
	            this.kcTokenParsed = {};
	            this.kcRefreshToken = "";
	            this.timeSkew = 0;
	            window.localStorage.removeItem('keycloakToken');
	            window.localStorage.removeItem('keycloakRefreshToken');

	            var req = new XMLHttpRequest();
	            req.open('POST', this.urls.logout, true);
	            req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	            req.setRequestHeader('Authorization', 'Basic ' + btoa(this.clientId + ':' + this.clientSecret));
	            req.withCredentials = true;

	            req.onreadystatechange = function () {
	                if (req.readyState == 4) {
	                    _this.$rootScope.$broadcast("keycloakLogout");
	                    if (req.status == 200) {
	                        deferred.resolve();
	                    } else {
	                        deferred.reject();
	                    }
	                    var state = angular.isFunction(_this.afterLogoutState) ? _this.afterLogoutState() : _this.afterLogoutState;
	                    _this.go(state);
	                }
	            };
	            req.send(param);
	            return deferred.promise;
	        }
	    }, {
	        key: "showLegalNotices",
	        value: function showLegalNotices() {
	            this.$state.go("menu.keycloakTermsConditions");
	        }
	    }, {
	        key: "register",
	        value: function register(user) {
	            if (!this.authenticated) {
	                this.loginMessage = undefined;
	                this.$state.go("keycloakRegister");
	            }
	        }
	    }, {
	        key: "setAfterLoginState",
	        value: function setAfterLoginState(afterLoginState) {
	            this.afterLoginState = afterLoginState;
	        }
	    }, {
	        key: "setAfterLogoutState",
	        value: function setAfterLogoutState(afterLogoutState) {
	            this.afterLogoutState = afterLogoutState;
	        }
	    }, {
	        key: "start",
	        value: function start() {
	            var _this2 = this;

	            var keycloakToken = window.localStorage.getItem("keycloakToken");
	            var keycloakRefreshToken = window.localStorage.getItem("keycloakRefreshToken");

	            if (keycloakRefreshToken === null || keycloakRefreshToken === 'undefined') {
	                this.startPromise.resolve();
	                return;
	            }
	            //If the access token is still valid, we use it
	            var tokenParsed = this.decodeToken(keycloakRefreshToken);
	            var timeSkew = Math.floor(new Date().getTime() / 1000) - tokenParsed.iat;
	            if (!this.isTokenExpired(60, tokenParsed.exp, timeSkew)) {
	                this.onAuthSuccess(keycloakToken, keycloakRefreshToken);
	            }

	            //If we know a refresh token, we try to use it to get a new access token
	            var refreshTokenParsed = this.decodeToken(keycloakRefreshToken);
	            timeSkew = Math.floor(new Date().getTime() / 1000) - refreshTokenParsed.iat;

	            this.refreshToken(-1, refreshTokenParsed.exp, timeSkew, keycloakRefreshToken).finally(function () {
	                _this2.startPromise.resolve();
	            });
	        }
	    }, {
	        key: "tokenParsed",
	        value: function tokenParsed() {
	            return this.kcTokenParsed;
	        }
	    }, {
	        key: "updateToken",
	        value: function updateToken() {
	            var minValidity = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : -1;

	            return this.refreshToken(minValidity, this.kcTokenParsed.exp, this.timeSkew, this.kcRefreshToken);
	        }

	        // ################## private ##################

	    }, {
	        key: "createUser",
	        value: function createUser(user) {
	            var deferred = this.$q.defer();
	            if (user.pseudo === "" || user.pseudo === null) {
	                user.pseudo = user.email;
	            }
	            var req = new XMLHttpRequest();
	            req.open('POST', this.urls.register, true);
	            req.setRequestHeader('Content-type', 'application/json');
	            req.setRequestHeader('Client-Id', this.clientId);
	            req.setRequestHeader('Authorization', 'Basic ' + btoa(this.clientId + ':' + this.clientSecret));
	            req.withCredentials = true;

	            req.onreadystatechange = function () {
	                if (req.readyState == 4) {
	                    if (req.status == 200) {
	                        deferred.resolve(JSON.parse(req.responseText));
	                    } else {
	                        deferred.reject(req.responseText === '' ? '' : JSON.parse(req.responseText));
	                    }
	                }
	            };
	            req.send(JSON.stringify(user));

	            return deferred.promise;
	        }
	    }, {
	        key: "decodeToken",
	        value: function decodeToken(str) {
	            str = str.split('.')[1];
	            str = str.replace('/-/g', '+');
	            str = str.replace('/_/g', '/');
	            switch (str.length % 4) {
	                case 0:
	                    break;
	                case 2:
	                    str += '==';
	                    break;
	                case 3:
	                    str += '=';
	                    break;
	                default:
	                    throw 'Invalid token';
	            }

	            str = (str + '===').slice(0, str.length + str.length % 4);
	            str = str.replace(/-/g, '+').replace(/_/g, '/');

	            str = decodeURIComponent(escape(atob(str)));

	            str = JSON.parse(str);
	            return str;
	        }
	    }, {
	        key: "login",
	        value: function login(username, password, afterRegister) {
	            var _this3 = this;

	            var deferred = this.$q.defer();

	            var req = new XMLHttpRequest();
	            req.open('POST', this.urls.token, true);
	            req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	            req.setRequestHeader('Authorization', 'Basic ' + btoa(this.clientId + ':' + this.clientSecret));
	            req.withCredentials = true;

	            req.onreadystatechange = function () {
	                if (req.readyState == 4) {
	                    if (req.status == 200) {
	                        var data = JSON.parse(req.responseText);
	                        _this3.onAuthSuccess(data.access_token, data.refresh_token, afterRegister);
	                        deferred.resolve();
	                    } else {
	                        deferred.reject(req.responseText === '' ? '' : JSON.parse(req.responseText));
	                    }
	                }
	            };
	            req.send('grant_type=password&username=' + encodeURIComponent(username) + '&password=' + encodeURIComponent(password));
	            return deferred.promise;
	        }
	    }, {
	        key: "forgotPassword",
	        value: function forgotPassword(email) {
	            var deferred = this.$q.defer();

	            var req = new XMLHttpRequest();
	            req.open('POST', this.urls.forgotPassword, true);
	            req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	            req.setRequestHeader('Authorization', 'bearer ' + this.token);
	            req.withCredentials = true;

	            req.onreadystatechange = function () {
	                if (req.readyState == 4) {
	                    if (req.status == 200) {
	                        var data = JSON.parse(req.responseText);
	                        deferred.resolve();
	                    } else {
	                        deferred.reject(req.responseText === '' ? '' : JSON.parse(req.responseText));
	                    }
	                }
	            };
	            req.send('email=' + encodeURIComponent(email));
	            return deferred.promise;
	        }
	    }, {
	        key: "isTokenExpired",
	        value: function isTokenExpired(minValidity, exp, timeSkew) {
	            if (minValidity === -1) {
	                return true;
	            }
	            var expiresIn = exp - Math.ceil(new Date().getTime() / 1000) + timeSkew;
	            if (minValidity) {
	                expiresIn -= minValidity;
	            }
	            return expiresIn < 0;
	        }
	    }, {
	        key: "refreshToken",
	        value: function refreshToken(minValidity, exp, timeSkew, _refreshToken) {
	            var _this4 = this;

	            var deferred = this.$q.defer();

	            //If the user is not authenticated
	            if (_refreshToken === '') {
	                deferred.reject();
	                return deferred.promise;
	            }
	            //If the access token is still valid
	            if (!this.isTokenExpired(minValidity, exp, timeSkew)) {
	                deferred.resolve();
	                return deferred.promise;
	            }

	            var req = new XMLHttpRequest();
	            req.open('POST', this.urls.token, true);
	            req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	            req.setRequestHeader('Authorization', 'Basic ' + btoa(this.clientId + ':' + this.clientSecret));
	            req.withCredentials = true;
	            req.timeout = 2000;

	            req.onreadystatechange = function () {
	                if (req.readyState == 4) {
	                    if (req.status == 200) {
	                        var data = JSON.parse(req.responseText);
	                        _this4.onAuthSuccess(data.access_token, data.refresh_token);
	                        deferred.resolve();
	                    } else {
	                        if (req.status >= 400 && req.status < 500) {
	                            _this4.logout();
	                        }
	                        if (minValidity === -1 && req.status < 400) {
	                            _this4.refreshToken(3600, exp, timeSkew, _refreshToken).then(function (data) {
	                                deferred.resolve();
	                            }, function () {
	                                deferred.reject();
	                            });
	                        } else {
	                            deferred.reject(req.responseText === '' ? '' : JSON.parse(req.responseText));
	                        }
	                    }
	                }
	            };
	            req.send('grant_type=refresh_token&refresh_token=' + encodeURIComponent(_refreshToken));
	            return deferred.promise;
	        }
	    }, {
	        key: "onAuthSuccess",
	        value: function onAuthSuccess(token, refreshToken) {
	            var afterRegister = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

	            var broadcastSignal = !this.authenticated ? true : false;
	            this.authenticated = true;
	            this.kcToken = token;
	            this.kcRefreshToken = refreshToken;
	            this.kcTokenParsed = this.decodeToken(token);
	            this.timeSkew = Math.floor(new Date().getTime() / 1000) - this.kcTokenParsed.iat;

	            this.userInfo.email = this.kcTokenParsed.email;
	            this.userInfo.pseudo = this.kcTokenParsed.preferred_username;
	            this.userInfo.firstName = this.kcTokenParsed.given_name;
	            this.userInfo.lastName = this.kcTokenParsed.family_name;
	            this.userInfo.id = this.kcTokenParsed.sub;

	            window.localStorage.setItem('keycloakToken', this.kcToken);
	            window.localStorage.setItem('keycloakRefreshToken', this.kcRefreshToken);

	            if (broadcastSignal) {
	                if (this.termsConditions !== null) {
	                    this.checkTermsConditionsVersion(afterRegister);
	                } else {
	                    this.showFirstState(afterRegister);
	                }
	            }
	        }
	    }, {
	        key: "checkTermsConditionsVersion",
	        value: function checkTermsConditionsVersion() {
	            var _this5 = this;

	            var afterRegister = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

	            var req = new XMLHttpRequest();
	            req.open('GET', this.urls.termsConditionsVersion, true);

	            req.onreadystatechange = function () {
	                if (req.readyState == 4) {
	                    if (req.status == 200) {
	                        var localVersion = window.localStorage.getItem("keycloakTermsConditionsValidatedVersion-" + _this5.userInfo.id);
	                        var data = JSON.parse(req.responseText);
	                        if (data[_this5.termsConditions.versionField] != localVersion) {
	                            _this5.termsConditions.newVersion = data[_this5.termsConditions.versionField];
	                            _this5.termsConditions.afterRegister = afterRegister;
	                            _this5.go('keycloakTermsConditions');
	                            return;
	                        }
	                    }
	                    _this5.showFirstState(afterRegister);
	                }
	            };
	            req.send();
	        }
	    }, {
	        key: "showFirstState",
	        value: function showFirstState(afterRegister) {
	            this.$rootScope.$broadcast("keycloakAuthenticated");
	            var state = '';
	            if (afterRegister) {
	                state = angular.isFunction(this.afterRegisterState) ? this.afterRegisterState() : this.afterRegisterState;
	            } else {
	                state = angular.isFunction(this.afterLoginState) ? this.afterLoginState() : this.afterLoginState;
	            }
	            this.go(state);
	        }
	    }, {
	        key: "go",
	        value: function go(state) {
	            var _this6 = this;

	            this.$timeout(function () {
	                _this6.$ionicHistory.clearHistory();
	                _this6.$ionicHistory.nextViewOptions({ historyRoot: true, disableBack: true });
	                _this6.$state.go(state);
	            });
	        }
	    }]);

	    return KeycloakMobileService;
	}();

	exports.KeycloakMobileService = KeycloakMobileService;

/***/ }
/******/ ]);