/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(5);


/***/ },

/***/ 5:
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var KeycloakInterceptor = exports.KeycloakInterceptor = function () {
	    function KeycloakInterceptor($q, $injector, backendUrl) {
	        _classCallCheck(this, KeycloakInterceptor);

	        this.$q = $q;
	        this.backendUrl = backendUrl;
	        this.$injector = $injector;
	        this.keycloakService = null;

	        return {
	            request: this.request.bind(this)
	        };
	    }

	    _createClass(KeycloakInterceptor, [{
	        key: 'request',
	        value: function request(config) {
	            if (this.keycloakService === null) {
	                //To avoid circular dependancies
	                this.keycloakService = this.$injector.get('keycloakService');
	            }
	            if (config.url.indexOf(this.backendUrl) > -1) {
	                angular.extend(config.headers, this.keycloakService.getHeaders());
	            }

	            return config;
	        }
	    }], [{
	        key: 'create',
	        value: function create($q, $injector) {
	            return new KeycloakInterceptor($q, $injector);
	        }
	    }]);

	    return KeycloakInterceptor;
	}();

	var KeycloakInterceptorProvider = exports.KeycloakInterceptorProvider = function () {
	    KeycloakInterceptorProvider.$inject = ["$httpProvider"];
	    function KeycloakInterceptorProvider($httpProvider) {
	        'ngInject';

	        _classCallCheck(this, KeycloakInterceptorProvider);

	        this.$httpProvider = $httpProvider;
	        this.urls = [];
	    }

	    _createClass(KeycloakInterceptorProvider, [{
	        key: 'setBackendUrl',
	        value: function setBackendUrl(url) {
	            this.backendUrl = url;
	        }
	    }, {
	        key: 'install',
	        value: function install() {
	            this.$httpProvider.interceptors.push('keycloakInterceptor');
	        }
	    }, {
	        key: '$get',
	        value: ["$q", "$injector", function $get($q, $injector) {
	            'ngInject';

	            return new KeycloakInterceptor($q, $injector, this.backendUrl);
	        }]
	    }]);

	    return KeycloakInterceptorProvider;
	}();

/***/ }

/******/ });