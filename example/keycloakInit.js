angular.element(document).ready(() => {
  window._keycloak = Keycloak('app/config/keycloak.json');

  window._keycloak
      .init({
        onLoad: 'check-sso'
      })
      .success(() => {
        angular.bootstrap(document, ['your-app'], {
          strictDi: true
        });
      });
});
